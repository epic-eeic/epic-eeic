/*global module: false, require: false*/

module.exports = function( grunt ) {
    "use strict";

    /**
     * Meta: only list tasks we care about
     */
    grunt.registerMultiTask(
        "tasks",
        "List tasks we care about (and only those)",
        function() {
            grunt.log.writeln();
            grunt.log.writeln( "######### TASK LIST STARTS #########" );
            grunt.log.writeln( "grunt" );
            grunt.log.writeln( "grunt server" );
            grunt.log.writeln();
            grunt.log.writeln( "grunt canada_apps" );
            grunt.log.writeln( "grunt canada_apps_unmin" );
            grunt.log.writeln( "grunt canada_transactional_apps" );
            grunt.log.writeln( "grunt custom" );
            grunt.log.writeln( "grunt custom_apps" );
            grunt.log.writeln( "grunt ic_sub_sites" );
            grunt.log.writeln( "grunt ic_sub_sites_apps" );
            grunt.log.writeln( "grunt canada_sites" );
            grunt.log.writeln( "grunt canada_sites_gcw_v5" );
            grunt.log.writeln( "grunt indie" );
            grunt.log.writeln( "grunt intranet" );
            grunt.log.writeln( "grunt intranet_apps" );
            grunt.log.writeln( "grunt icweb" );
            grunt.log.writeln( "grunt intranet_icweb_apps" );
            grunt.log.writeln( "######### TASK LIST ENDS #########" );
            grunt.log.writeln();
        }
    );

    /**
     * Print url
     */
    grunt.registerTask(
        "print_url",
        "Description: TODO",
        function( taskname ) {
            var prefix = "http://127.0.0.1:8080/eic/templates/",
                suffix = "/localhost/page-list-eng.html",
                tpl;

            switch ( taskname ) {
                case "canada_apps":
                    tpl = "Canada_Apps";
                    break;
                case "canada_apps_unmin":
                    tpl = "Canada_Apps_Unmin";
                    break;
                case "canada_transactional_apps":
                    tpl = "Canada_Transactional_Apps";
                    break;
                case "custom":
                    tpl = "Custom";
                    break;
                case "custom_apps":
                    tpl = "Custom_Apps";
                    break;
                case "ic_sub_sites":
                    tpl = "IC_Sub_Sites";
                    break;
                case "ic_sub_sites_apps":
                    tpl = "IC_Sub_Sites_Apps";
                    break;
                case "canada_sites":
                    tpl = "Canada_Sites";
                    break;
                case "canada_sites_gcw_v5":
                    tpl = "Canada_Sites_GCW_V5";
                    break;
                case "indie":
                    tpl = "Indie";
                    break;
                case "intranet":
                    tpl = "Intranet";
                    break;
                case "intranet_apps":
                    tpl = "Intranet_Apps";
                    break;
                case "icweb":
                    tpl = "Intranet_ICWeb";
                    break;
                case "icweb_apps":
                    tpl = "Intranet_ICWeb_Apps";
                    break;
                default:
                    return;
            }

            grunt.log.subhead( "Access the local environment via: " + prefix + tpl + suffix );
        }
    );

    /**
     * External Tasks (show up in brackets-grunt)
     *
     * These tasks show up in brackets-grunt since their descriptions
     * start with "PUBLIC: "
     *
     * A modified version of the "brackets-grunt" plugin ( https://github.com/dhategan/brackets-grunt ) is required
     * It can be found here: https://bitbucket.org/epic-eeic/brackets-grunt
     *
     * This is just to keep the panel clean.
     */
    grunt.registerTask(
        "default",
        "PUBLIC: Default task that runs the production build",
        [
            "dist"
        ]
    );

    grunt.registerTask(
        "canada_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:canada_apps",
            "assemble:canada_apps",
            "htmllint:canada_apps",
            "print_url:canada_apps"
        ]
    );

    grunt.registerTask(
        "canada_apps_unmin",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:canada_apps_unmin",
            "assemble:canada_apps_unmin",
            "htmllint:canada_apps_unmin",
            "print_url:canada_apps_unmin"
        ]
    );

    grunt.registerTask(
        "canada_transactional_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:canada_transactional_apps",
            "assemble:canada_transactional_apps",
            "htmllint:canada_transactional_apps",
            "print_url:canada_transactional_apps"
        ]
    );

    grunt.registerTask(
        "custom",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:custom",
            "assemble:custom",
            "htmllint:custom",
            "print_url:custom"
        ]
    );

    grunt.registerTask(
        "custom_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:custom_apps",
            "assemble:custom_apps",
            "htmllint:custom_apps",
            "print_url:custom_apps"
        ]
    );

    grunt.registerTask(
        "ic_sub_sites",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:ic_sub_sites",
            "assemble:ic_sub_sites",
            "htmllint:ic_sub_sites",
            "print_url:ic_sub_sites"
        ]
    );

    grunt.registerTask(
        "ic_sub_sites_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:ic_sub_sites_apps",
            "assemble:ic_sub_sites_apps",
            "htmllint:ic_sub_sites_apps",
            "print_url:ic_sub_sites_apps"
        ]
    );

    grunt.registerTask(
        "canada_sites",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:canada_sites",
            "assemble:canada_sites",
            "htmllint:canada_sites",
            "print_url:canada_sites"
        ]
    );

    grunt.registerTask(
        "canada_sites_gcw_v5",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:canada_sites_gcw_v5",
            "assemble:canada_sites_gcw_v5",
            "htmllint:canada_sites_gcw_v5",
            "print_url:canada_sites_gcw_v5"
        ]
    );

    grunt.registerTask(
        "indie",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:indie",
            "assemble:indie",
            "htmllint:indie",
            "print_url:indie"
        ]
    );

    grunt.registerTask(
        "intranet",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:intranet",
            "assemble:intranet",
            "htmllint:intranet",
            "print_url:intranet"
        ]
    );

    grunt.registerTask(
        "intranet_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:intranet_apps",
            "assemble:intranet_apps",
            "htmllint:intranet_apps",
            "print_url:intranet_apps"
        ]
    );

    grunt.registerTask(
        "icweb",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:icweb",
            "assemble:icweb",
            "htmllint:icweb",
            "print_url:icweb"
        ]
    );

    grunt.registerTask(
        "icweb_apps",
        "PUBLIC: Default task that runs the production build",
        [
            "checkDependencies",
            "test",
            "build",
            "string-replace:intranet_icweb_apps",
            "assemble:intranet_icweb_apps",
            "htmllint:intranet_icweb_apps",
            "print_url:icweb_apps"
        ]
    );

    grunt.registerTask(
        "server",
        "PUBLIC: Run the Connect web server for local repo",
        [
            "connect:server:keepalive"
        ]
    );

    grunt.registerTask(
        "debug",
        "PUBLIC: Produces unminified files",
        [
            "build",
            "demos"
        ]
    );

    grunt.registerTask(
        "fetch",
        "PUBLIC: Fetch a page from URL then build",
        [
            "wget:url"
        ]
    );

    /**
     * Internal Tasks
     *
     * These tasks won't show up in brackets-grunt since their descriptions
     * don't start with "PUBLIC: "
     *
     * A modified version of the "brackets-grunt" plugin ( https://github.com/dhategan/brackets-grunt ) is required
     * It can be found here: https://bitbucket.org/epic-eeic/brackets-grunt
     *
     * This is just to keep the panel clean.
     */
    grunt.registerTask(
        "dist",
        "Produces the production files",
        [
            "checkDependencies",
            "test",
            "build",
            "demos",
            "htmllint:all",
            "htmllint:canada_transactional_apps"
        ]
    );

    grunt.registerTask(
        "build",
        "Run full build.",
        [
            "clean",
            "fontello_svg",
            "webfont",
            "css",
            "js",
            "copy",
            "imagemin"
        ]
    );

    grunt.registerTask(
        "js",
        "Copies all third party JS to the dist folder",
        [
            "concat",
            "uglify"
        ]
    );

    grunt.registerTask(
        "css",
        "Compiles Sass and copies third party CSS to the dist folder",
        [
            "string-replace:fonts",
            "sass",
            "autoprefixer",
            "csslint",
            "cssmin"
        ]
    );

    grunt.registerTask(
        "demos",
        "Create unminified demos",
        [
            "string-replace",
            "assemble"
        ]
    );

    grunt.registerTask(
        "test",
        "Runs testing tasks",
        [
            "eslint",
            "sasslint"
        ]
    );

    // Util
    grunt.util.linefeed = "\n";

    // Config
    grunt.initConfig( {
        pkg: grunt.file.readJSON( "package.json" ),

        // Necessary for our tasks "registerMultiTask" above.
        tasks: {
            dummy: {}
        },

        /**
         * Replace EPIC "variables" in master templates with handlebars includes
         *
         * This task currently runs on build\**\templates\*.tpl
         * It generates proper Handlebars (http://handlebarsjs.com/) templates (.hbs)
         *
         * It looks for a pattern like: "<!-- @import src/Custom/1-Master Templates/0-dtd-eng.html -->"
         * And extracts the path (ex: "src/Custom/1-Master Templates/0-dtd-eng.html")
         *
         * It will then replace the "@import" line with the content in the file specified in the path
         * The content of those files should be identical to the Master Templates in EPIC
         *
         * Before placing the imported content in the .hbs files, it will replace EPIC variables (ex: !MENU!)
         * with hbs includes ( ex: {{>epic.megamenu-eng}} ).
         *
         * The hbs includes are located in build\**\includes\**\*
         */
        "string-replace": {
            options: {
                replacements: [ {
                    pattern: /<!-- @import (.*?) -->/ig,
                    replacement: function( match, p1 ) {
                        var data = grunt.file.read( p1 ),
                            type = p1.match( /(-dtd-|-head-|-header-|-config-|-footer-)/ ),
                            lang = ( p1.match( /-fra\.html$/ ) === null ) ? "eng" : "fra";

                        if ( type !== null ) {
                            type = type[ 1 ].replace( /-/g, "" );

                            switch ( type ) {
                                case "header":
                                    data = data.replace( "!MENU!", "{{>epic.megamenu-" + lang + "}}" );
                                    data = data.replace( "!LANG.SWAP!", "{{>epic.language-toggle-" + lang + "}}" );
                                    break;
                                case "config":
                                    data = data.replace( "!MENU!", "{{>epic.leftnav-" + lang + "}}" );
                                    data = data.replace( "!BODY!", "{{>body}}" );
                                    data = data.replace( "!BREADCRUMB!", "{{>epic.breadcrumb-" + lang + "}}" );
                                    break;
                                case "footer":
                                    data = data.replace( "!LINKS!", "{{>epic.links-" + lang + "}}" );
                                    data = data.replace( "!PAGEID!", "{{>epic.pageid-" + lang + "}}" );
                                    break;
                            }

                            data = data.replace( /!([A-Z0-9\.]+)!/g, "{{{ " + type + ".$1 }}}" );
                        }

                        return data;
                    }
                } ]
            },
            fonts: {
                options: {
                    replacements: [ {
                        pattern: /(?:src:)?url\("\/eic\/home\.nsf\/a\/(eic-icon\.[a-z]{3}[a-z]?)/g,
                        replacement: function( match, p1 ) {
                            return match + "/$file/" + p1;
                        }
                    } ]
                },
                files: [ {
                    expand: true,
                    cwd: "src/All/2-CSS/",
                    src: "_eic-icon.scss",
                    dest: "src/All/2-CSS/"
                } ]
            },
            canada_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/Canada_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Canada_Apps/layout/"
                } ]
            },
            canada_apps_unmin: {
                files: [ {
                    expand: true,
                    cwd: "build/Canada_Apps_Unmin/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Canada_Apps_Unmin/layout/"
                } ]
            },
            canada_transactional_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/Canada_Transactional_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Canada_Transactional_Apps/layout/"
                } ]
            },
            custom: {
                files: [ {
                    expand: true,
                    cwd: "build/Custom/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Custom/layout/"
                } ]
            },
            custom_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/Custom_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Custom_Apps/layout/"
                } ]
            },
            ic_sub_sites: {
                files: [ {
                    expand: true,
                    cwd: "build/IC_Sub_Sites/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/IC_Sub_Sites/layout/"
                } ]
            },
            ic_sub_sites_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/IC_Sub_Sites_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/IC_Sub_Sites_Apps/layout/"
                } ]
            },
            canada_sites: {
                files: [ {
                    expand: true,
                    cwd: "build/Canada_Sites/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Canada_Sites/layout/"
                } ]
            },
            canada_sites_gcw_v5: {
                files: [ {
                    expand: true,
                    cwd: "build/Canada_Sites_GCW_V5/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Canada_Sites_GCW_V5/layout/"
                } ]
            },
            indie: {
                files: [ {
                    expand: true,
                    cwd: "build/Indie/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Indie/layout/"
                } ]
            },
            intranet: {
                files: [ {
                    expand: true,
                    cwd: "build/Intranet/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Intranet/layout/"
                } ]
            },
            intranet_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/Intranet_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Intranet_Apps/layout/"
                } ]
            },
            icweb: {
                files: [ {
                    expand: true,
                    cwd: "build/Intranet_ICWeb/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Intranet_ICWeb/layout/"
                } ]
            },
            icweb_apps: {
                files: [ {
                    expand: true,
                    cwd: "build/Intranet_ICWeb_Apps/templates/",
                    src: "*.tpl",
                    ext: ".hbs",
                    dest: "build/Intranet_ICWeb_Apps/layout/"
                } ]
            }
        },

        /**
         * Copy files, as-is, from one location to another
         *
         * Used to copy:
         *  - Images
         *      This process will generate the proper EPIC path.
         *      ex: "src/All/3-Images/itb/foo.png" will become "demos/eic/home.nsf/images/foo.png/$file/foo.png"
         *
         *  - HTML
         *      This is meant to be equivalent to the "Objects > HTML" section in EPIC
         *
         *  - JS
         *      Javascript files that we don't want to minimize (ex: 3rd-party libraries)
         */
        copy: {
            dbAssets: {
                expand: true,
                cwd: "db-assets",
                src: "*/**/*",
                dest: "demos/eic/site/",
                rename: function( dest, src ) {
                    var filename = /\/([^\/]+)$/.exec( src );

                    if ( filename !== null ) {
                        filename = filename[ 1 ] || "error.txt";
                    } else {
                        filename = "error.txt";
                    }

                    return dest + src + "/$file/" + filename;
                }
            },
            images: {
                expand: true,
                src: "src/**/3-Images/**/*.*",
                dest: "demos/eic/home.nsf/images/",
                rename: function( dest, src ) {
                    return dest + src + "/$file/" + src;
                },
                flatten: true
            },
            html: {
                expand: true,
                src: "src/**/5-HTML/**/*.*",
                dest: "demos/eic/home.nsf/html/",
                flatten: true
            },
            js: {
                expand: true,
                src: "src/**/4-Javascript/lib/**/*.js",
                dest: "demos/eic/home.nsf/js/",
                flatten: true
            },
            fonts: {
                expand: true,
                src: "demos/eic/home.nsf/fonts/*",
                dest: "demos/eic/home.nsf/a/",
                rename: function( dest, src ) {
                    return dest + src + "/$file/" + src;
                },
                flatten: true
            }
        },

        /**
         * Build HTML pages
         *
         * Uses Assemble: http://assemble.io/docs/
         *
         * The "rename" statement makes sure we keep the page's parent folder
         * in the destination location (usually "eng" or "fra")
         *
         * ex: "src/Custom/pages/eng/1col.html" will become "demos/eic/templates/Custom/eng/1col.html"
         */
        assemble: {
            options: {
                prettify: {
                    indent: 2
                },
                layout: "1col-eng.hbs"
            },
            canada_apps: {
                options: {
                    data: "build/Canada_Apps/data.yml",
                    layoutdir: "build/Canada_Apps/layout",
                    partials: "build/Canada_Apps/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Canada_Apps/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Canada_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Canada_Apps/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            canada_apps_unmin: {
                options: {
                    data: "build/Canada_Apps_Unmin/data.yml",
                    layoutdir: "build/Canada_Apps_Unmin/layout",
                    partials: "build/Canada_Apps_Unmin/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Canada_Apps_Unmin/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Canada_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Canada_Apps_Unmin/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            canada_transactional_apps: {
                options: {
                    data: "build/Canada_Transactional_Apps/data.yml",
                    layoutdir: "build/Canada_Transactional_Apps/layout",
                    partials: "build/Canada_Transactional_Apps/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Canada_Transactional_Apps/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Canada_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Canada_Transactional_Apps/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            custom: {
                options: {
                    data: "build/Custom/data.yml",
                    layoutdir: "build/Custom/layout",
                    partials: "build/Custom/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Custom/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Custom/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            custom_apps: {
                options: {
                    data: "build/Custom_Apps/data.yml",
                    layoutdir: "build/Custom_Apps/layout",
                    partials: "build/Custom_Apps/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Custom_Apps/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Custom_Apps/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            ic_sub_sites: {
                options: {
                    data: "build/IC_Sub_Sites/data.yml",
                    layoutdir: "build/IC_Sub_Sites/layout",
                    partials: "build/IC_Sub_Sites/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/IC_Sub_Sites/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/IC_Sub_Sites/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            canada_sites: {
                options: {
                    data: "build/Canada_Sites/data.yml",
                    layoutdir: "build/Canada_Sites/layout",
                    partials: "build/Canada_Sites/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Canada_Sites/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Canada_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Canada_Sites/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            canada_sites_gcw_v5: {
                options: {
                    data: "build/canada_sites_gcw_v5/data.yml",
                    layoutdir: "build/canada_sites_gcw_v5/layout",
                    partials: "build/canada_sites_gcw_v5/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Canada_Sites_GCW_V5/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Canada_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Canada_Sites_GCW_V5/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            indie: {
                options: {
                    data: "build/Indie/data.yml",
                    layoutdir: "build/Indie/layout",
                    partials: "build/Indie/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Indie/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Indie/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            intranet: {
                options: {
                    data: "build/Intranet/data.yml",
                    layoutdir: "build/Intranet/layout",
                    partials: "build/Intranet/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Intranet/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Intranet_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Intranet/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            intranet_apps: {
                options: {
                    data: "build/Intranet_Apps/data.yml",
                    layoutdir: "build/Intranet_Apps/layout",
                    partials: "build/Intranet_Apps/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Intranet_Apps/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Intranet_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Intranet_Apps/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            icweb: {
                options: {
                    data: "build/Intranet_ICWeb/data.yml",
                    layoutdir: "build/Intranet_ICWeb/layout",
                    partials: "build/Intranet_ICWeb/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Intranet_ICWeb/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Intranet_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Intranet_ICWeb/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            icweb_apps: {
                options: {
                    data: "build/Intranet_ICWeb_Apps/data.yml",
                    layoutdir: "build/Intranet_ICWeb_Apps/layout",
                    partials: "build/Intranet_ICWeb_Apps/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        src: [
                            "src/Intranet_ICWeb_Apps/pages/**/*.html",
                            "src/All/pages/**/*.html",
                            "src/Intranet_All/pages/**/*.html",
                            "build/localhost/page-list-eng.html"
                        ],
                        dest: "demos/eic/templates/Intranet_ICWeb_Apps/",
                        rename: function( destBase, destPath ) {
                            return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
                        }
                    }
                ]
            },
            site: {
                options: {
                    data: "build/Canada_Sites/data.yml",
                    layoutdir: "build/Canada_Sites/layout",
                    partials: "build/Canada_Sites/includes/**/*.html"
                },
                files: [
                    {
                        expand: true,
                        cwd: "build/localhost",
                        src: "**/*.html",
                        dest: "demos/"
                    }
                ]
            }
        },

        print_url: {
            all: {
                url: "http://127.0.0.1:8080"
            },
            canada_apps: {
                url: "http://127.0.0.1:8080/eic/templates/Canada_Apps/localhost/page-list-eng.html"
            }
        },

        // Concatenate all JS files into one
        concat: {
            utils: {
                src: [
                    "src/All/4-Javascript/**/*.js",
                    "!src/All/4-Javascript/lib/*.js",
                    "!src/All/4-Javascript/surveys-v3.js"
                ],
                dest: "demos/eic/home.nsf/js/_WET_4-0_utils.unmin.js"
            },
            ic_subsites: {
                src: [
                    "src/IC_Sub_Sites/4-Javascript/*.js"
                ],
                dest: "demos/eic/home.nsf/js/ic_WET_4-0_institution.unmin.js"
            },
            canada: {
                src: [
                    "src/All/4-Javascript/webtrends.js",
                    "src/All/4-Javascript/basic.js",
                    "src/All/4-Javascript/ff-fixes.js",
                    "src/All/4-Javascript/responsive-breadcrumbs.js",
                    "src/All/4-Javascript/ajax-form.js",
                    "src/Canada_All/4-Javascript/*.js"
                ],
                dest: "demos/eic/home.nsf/js/_WET_4-0_utils_canada.unmin.js"
            },
            loop11_partners: {
                src: [
                    "src/All/4-Javascript/cookies.js",
                    "src/All/4-Javascript/surveys.js"
                ],
                dest: "demos/eic/home.nsf/js/loop11.unmin.js"
            },
            loop11_partners_v3: {
                src: [
                    "src/All/4-Javascript/cookies.js",
                    "src/All/4-Javascript/surveys-v3.js"
                ],
                dest: "demos/eic/home.nsf/js/loop11-v3.unmin.js"
            },
            icweb_widgets: {
                src: [
                    "src/Intranet_ICWeb/4-Javascript/dashboard/*.js",
                    "src/Intranet_ICWeb/4-Javascript/dashboard/widgets/*.js"
                ],
                dest: "db-assets/icweb.nsf/vwapj/widgets.unmin.js"
            }
        },

        // Convert SCSS to CSS
        sass: {
            utils: {
                expand: true,
                flatten: true,
                src: [ "src/All/2-CSS/**/*.scss" ],
                dest: "demos/eic/home.nsf/css/",
                ext: ".css"
            },
            ic_subsites: {
                expand: true,
                flatten: true,
                src: [ "src/IC_Sub_Sites/2-CSS/**/*.scss" ],
                dest: "demos/eic/home.nsf/css/",
                ext: ".css"
            },
            intranet_utils: {
                expand: true,
                flatten: true,
                src: [ "src/Intranet_All/2-CSS/**/*.scss" ],
                dest: "demos/eic/home.nsf/css/",
                ext: ".css"
            },
            canada_utils: {
                expand: true,
                flatten: true,
                src: [ "src/Canada_All/2-CSS/**/*.scss" ],
                dest: "demos/eic/home.nsf/css/",
                ext: ".css"
            }
        },

        // Add CSS browser prefixes when necessary
        autoprefixer: {
            modern: {
                options: {
                    browsers: [
                        "last 2 versions",
                        "android >= 2.3",
                        "bb >= 7",
                        "ff >= 17",
                        "ie > 8",
                        "ios 5",
                        "opera 12.1"
                    ]
                },
                cwd: "demos/eic/home.nsf/css",
                src: [ "**/*.css" ],
                dest: "demos/eic/home.nsf/css",
                expand: true
            }
        },

        // Lint CSS
        csslint: {
            options: {
                csslintrc: ".csslintrc"
            },

            ic: {
                options: {
                    absoluteFilePathsForFormatters: true,
                    formatters: [
                        {
                            id: "csslint-xml",
                            dest: "csslint-unmin.log"
                        }
                    ]
                },
                src: "demos/eic/**/*.css"
            }
        },

        // Minify JS
        uglify: {
            options: {
                sourceMap: true,
                maxLineLen: 3000
            },
            utils: {
                expand: true,
                cwd: "demos/eic/home.nsf/js",
                src: [ "_WET_4-0_utils.unmin.js" ],
                dest: "demos/eic/home.nsf/js/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            },
            ic_subsites: {
                expand: true,
                cwd: "demos/eic/home.nsf/js",
                src: [ "ic_WET_4-0_institution.unmin.js" ],
                dest: "demos/eic/home.nsf/js/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            },
            canada: {
                expand: true,
                cwd: "demos/eic/home.nsf/js",
                src: [ "_WET_4-0_utils_canada.unmin.js" ],
                dest: "demos/eic/home.nsf/js/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            },
            loop11_partners: {
                expand: true,
                cwd: "demos/eic/home.nsf/js",
                src: [ "loop11.unmin.js" ],
                dest: "demos/eic/home.nsf/js/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            },
            loop11_partners_v3: {
                expand: true,
                cwd: "demos/eic/home.nsf/js",
                src: [ "loop11-v3.unmin.js" ],
                dest: "demos/eic/home.nsf/js/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            },
            icweb_widgets: {
                expand: true,
                cwd: "db-assets/icweb.nsf/vwapj",
                src: [ "widgets.unmin.js" ],
                dest: "db-assets/icweb.nsf/vwapj/",
                rename: function( destBase, destPath ) {
                    return destBase + destPath.replace( /\.unmin\.js$/, ".min.js" );
                }
            }
        },

        // Minify CSS
        cssmin: {
            options: {
                keepBreaks: true
            },
            all: {
                cwd: "demos/eic/home.nsf/css",
                dest: "demos/eic/home.nsf/css",
                expand: true,
                ext: ".min.css",
                src: [ "*.css" ]
            }
        },

        // Get SVGs from fontello
        fontello_svg: {
            all: {
                options: {
                    css: false,
                    skip: true,
                    verbose: true,
                    fileFormat: "{1}.svg"
                },
                config: "src/All/svgs-for-fonts/from-fontello/fontello-config.json",
                dest: "src/All/svgs-for-fonts/from-fontello/"
            }
        },

        // Lint HTML
        htmllint: {
            options: {
                ignore: [
                    "The “details” element is not supported properly by browsers yet. It would probably be better to wait for implementations.",
                    "The “date” input type is not supported in all browsers. Please be sure to test, and consider using a polyfill.",
                    "The “time” input type is not supported in all browsers. Please be sure to test, and consider using a polyfill.",
                    "The “longdesc” attribute on the “img” element is obsolete. Use a regular “a” element to link to the description.",
                    "Bad value “X-UA-Compatible” for attribute “http-equiv” on XHTML element “meta”.",

                    /* Needed for "/src/All/pages/{eng,fra}/WET_4.x_Demo-of-all-design-features.html": multiple <h1> examples */
                    "Consider using the “h1” element as a top-level heading only (all “h1” elements are treated as top-level headings by many screen readers and other tools)."
                ]
            },
            all: {
                src: [
                    "demos/eic/templates/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html",
                    "!demos/eic/templates/Canada_Transactional_Apps/**/*.html"
                ]
            },
            canada_apps: {
                src: [
                    "demos/eic/templates/Canada_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            canada_apps_unmin: {
                src: [
                    "demos/eic/templates/Canada_Apps_Unmin/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            canada_transactional_apps: {
                options: {
                    ignore: [
                        "The “details” element is not supported properly by browsers yet. It would probably be better to wait for implementations.",
                        "The “date” input type is not supported in all browsers. Please be sure to test, and consider using a polyfill.",
                        "The “time” input type is not supported in all browsers. Please be sure to test, and consider using a polyfill.",
                        "The “longdesc” attribute on the “img” element is obsolete. Use a regular “a” element to link to the description.",
                        "Bad value “X-UA-Compatible” for attribute “http-equiv” on XHTML element “meta”.",

                        /* Needed for "/src/All/pages/{eng,fra}/WET_4.x_Demo-of-all-design-features.html": multiple <h1> examples */
                        "Consider using the “h1” element as a top-level heading only (all “h1” elements are treated as top-level headings by many screen readers and other tools).",

                        /* The "transactional" demo template from TBS triggers this */
                        /* ( https://ssl-templates.services.gc.ca/app/cls/WET/gcweb/v4_0_17/samples/transact-en.shtml )*/
                        "Section lacks heading. Consider using “h2”-“h6” elements to add identifying headings to all sections."
                    ]
                },
                src: [
                    "demos/eic/templates/Canada_Transactional_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            custom: {
                src: [
                    "demos/eic/templates/Custom/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            custom_apps: {
                src: [
                    "demos/eic/templates/Custom_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            ic_sub_sites: {
                src: [
                    "demos/eic/templates/IC_Sub_Sites/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            ic_sub_sites_apps: {
                src: [
                    "demos/eic/templates/IC_Sub_Sites_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            canada_sites: {
                src: [
                    "demos/eic/templates/Canada_sites/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            canada_sites_gcw_v5: {
                src: [
                    "demos/eic/templates/Canada_Sites_GCW_V5/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            indie: {
                src: [
                    "demos/eic/templates/Indie/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            intranet: {
                src: [
                    "demos/eic/templates/Intranet/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            intranet_apps: {
                src: [
                    "demos/eic/templates/Intranet_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            icweb: {
                src: [
                    "demos/eic/templates/Intranet_ICWeb/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            },
            icweb_apps: {
                src: [
                    "demos/eic/templates/Intranet_ICWeb_Apps/**/*.html",
                    "!demos/eic/templates/**/localhost/*.html"
                ]
            }
        },

        // Lint SASS
        sasslint: {
            options: {
                configFile: ".sass-lint.yml"
            },
            all: {
                src: "src/**/2-CSS/*.scss"
            }
        },

        // Optimize images
        imagemin: {
            all: {
                cwd: "dist/unmin",
                src: "**/*.png",
                dest: "dist/unmin",
                expand: true
            }
        },

        /**
         * Clean demo directories
         *
         * Delete the files/folders in the paths listed below
         */
        clean: {
            dist: [
                "demos/eic/templates/*",
                "demos/eic/site",
                "!demos/eic/templates/remote.html",
                "demos/eic/unmin-css/*",
                "demos/eic/home.nsf/**/*",
                "src/All/pages/eng/eic-icon.html"
            ],
            layouts: [
                "build/**/layout/*.hbs",
                "!build/**/layout/page-list.hbs" /* tmp */
            ],
            site: [
                "demos/*.html"
            ]
        },

        /**
         * Make grunt watch for changes in source files (Live Reload)
         *
         * This task can be run with the "grunt watch" command
         * and is useful with the "Live Reload" browser extension:
         * http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions-
         *
         * If this task is running, the following happens when a file under the "src" folder is modified and saved:
         *  - "grunt dist" will automatically run and thus update the local environment (build)
         *  - your browser window/tab will automatically refresh and show you the updated environment
         */
        watch: {
            source: {
                files: [ "src/**/*.*" ],
                tasks: "dist",
                options: {
                    interval: 5007,
                    livereload: true
                }
            }
        },

        // Lint JS
        eslint: {
            options: {
                configFile: ".eslintrc"
            },
            target: [
                "Gruntfile.js",
                "src/**/*.js",
                "!src/**/4-Javascript/lib/*.js"
            ]
        },

        // Make fonts out of SVGs
        webfont: {
            icons: {
                src: "src/All/svgs-for-fonts/**/*.svg",
                dest: "demos/eic/home.nsf/fonts",
                destCss: "src/All/2-CSS",
                options: {
                    stylesheet: "scss",
                    engine: "node",
                    autoHint: false,
                    hashes: false,
                    font: "eic-icon",
                    htmlDemoTemplate: "build/All/svg-fonts/html-demo-template.html",
                    destHtml: "src/All/pages/eng/",
                    relativeFontPath: "/eic/home.nsf/a",
                    templateOptions: {
                        baseClass: "eic-icon",
                        classPrefix: "eic-icon-"
                    }
                }
            }
        },

        /**
         * Local HTTP server
         *
         * This task can be run with the "grunt server" command
         * Navigating to http://127.0.0.1:8080 in your browser will show "demos/index.html"
         */
        connect: {
            options: {
                port: 8080
            },

            server: {
                options: {
                    base: "demos",
                    middleware: function( connect, options, middlewares ) {
                        middlewares.push( connect.compress( {
                            filter: function( req, res ) {
                                return ( /json|text|javascript|dart|image\/svg\+xml|application\/x-font-ttf|application\/vnd\.ms-opentype|application\/vnd\.ms-fontobject/ ).test( res.getHeader( "Content-Type" ) );
                            }
                        } ) );

                        // rewrite path so the leading "demos" folder is removed from URL
                        middlewares.push( function( req, res, next ) {
                            req.url = req.url.replace( /^\/demos\//, "/" );
                            return next();
                        } );

                        middlewares.push( function( req, res ) {
                            var filename = options.base + req.url;

                            if ( !grunt.file.exists( filename ) ) {
                                filename = options.base + "/404.html";
                                res.statusCode = 404;
                            }

                            return res.end( grunt.file.read( filename ) );
                        } );

                        return middlewares;
                    }
                }
            }
        },

        checkDependencies: {
            all: {
                options: {
                    npmInstall: false
                }
            }
        }
    } );

    // Load task libs
    grunt.loadNpmTasks( "assemble" );                     /* Build pages */
    grunt.loadNpmTasks( "grunt-autoprefixer" );           /* Add CSS browser-vendor prefixes where appropriate */
    grunt.loadNpmTasks( "grunt-check-dependencies" );     /* Check dependencies */
    grunt.loadNpmTasks( "grunt-contrib-clean" );          /* Delete files/dirs */
    grunt.loadNpmTasks( "grunt-contrib-concat" );         /* Concatenate multiple files together */
    grunt.loadNpmTasks( "grunt-contrib-connect" );        /* Local server */
    grunt.loadNpmTasks( "grunt-contrib-copy" );           /* Copy files from one place to another */
    grunt.loadNpmTasks( "grunt-contrib-csslint" );        /* Lint CSS */
    grunt.loadNpmTasks( "grunt-contrib-cssmin" );         /* Minify CSS */
    grunt.loadNpmTasks( "grunt-contrib-imagemin" );       /* Optimize images */
    grunt.loadNpmTasks( "grunt-contrib-uglify" );         /* Minify JS */
    grunt.loadNpmTasks( "grunt-contrib-watch" );          /* Auto-reload */
    grunt.loadNpmTasks( "grunt-html" );                   /* Lint HTML */
    grunt.loadNpmTasks( "grunt-eslint" );                 /* Lint JS */
    grunt.loadNpmTasks( "grunt-sass" );                   /* Convert SASS to CSS */
    grunt.loadNpmTasks( "grunt-sass-lint" );              /* Lint SCSS */
    grunt.loadNpmTasks( "grunt-string-replace" );         /* Search and replace */
    grunt.loadNpmTasks( "grunt-fontello-svg" );           /* Get SVGs from fontello.com */
    grunt.loadNpmTasks( "grunt-webfont" );                /* Make fonts out of SVGs */
    require( "time-grunt" )( grunt );                     /* Display the elapsed execution time of grunt tasks */
};
