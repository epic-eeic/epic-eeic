Installation
------------------------

NodeJS and Git are the build tools used by WET.  
Brackets is the editor we currently use.  
Java is necessary to run the "HTML Lint" (htmllint) grunt task, but that task  
doesn't need to be successful to build Production files (it's the last task in  
the "dist" target, after everything's been linted, minified, etc.)  

Required:

* [Install NodeJS](https://nodejs.org/) (tested with 6.10.3 LTS)
* [Install Git](http://git-scm.com/) (tested with 2.13.0)

Optionally, install:

* [Brackets](http://brackets.io/)
    * [brackets-git](https://github.com/zaggino/brackets-git)
    * [brackets-grunt](https://bitbucket.org/epic-eeic/brackets-grunt)
    * [brackets-file-tree-exclude](https://github.com/zaggino/brackets-file-tree-exclude)
    * [brackets-npm-registry](https://github.com/zaggino/brackets-npm-registry/)
        * [brackets-eslint](https://github.com/zaggino/brackets-eslint)
        * [brackets-sass-lint](https://github.com/petetnt/brackets-sass-lint)
    * [brackets-yaml-linter](https://github.com/slai/brackets-yaml-linter)

Since you are behind a proxy ( see: https://github.com/adobe/brackets/wiki/How-to-Use-Brackets#preferences ):

* Find your proxy settings:  
  open IE > Tools > Connections > LAN settings > advanced.
* Allow Brackets' Extension Manager to connect to the repository
    * Debug > Open Preferences File
    * Add the following, at the top of the file (brackets.json) immediately following the open bracket: (Use correct proxy information)
        "proxy": "http://somename.prod.prv:80",


Git Configuration
------------------------

1. Via the Git console, add your proxy information (found above) with these commands:  
       ```git config --global http.proxy http://proxy.server.com:8080```  
       ```git config --global https.proxy http://proxy.servers.com:8080```

(The proxy configuration for git is the same as brackets. E.G.: http://cdhwg01.prod.prv:80)

Project configuration (one folder, "git checkout" to change branches)
------------------------

1. Via "Brackets Git" (or regular "Git"), clone https://bitbucket.org/epic-eeic/epic-eeic.git


Alternate Project Configuration (separate folder for every branch)
------------------------

1. Create a new folder on C: ( E.G.: C:\worklocal\ )

2. Via "Brackets Git" (or regular "Git"), clone https://bitbucket.org/epic-eeic/epic-eeic.git into worklocal

3. In C:\worklocal, change foder name epic-eeic to v4.0

4. Create new folder for each version. ( E.G.: v2.3, v3.0, v3.1, v3.1.7 )

5. Open v4.0 folder, copy .git folder & paste it into each new folder.

6. In Brackets, Select branch dropdown ( E.G.: v4.0 ) > Open folder. Open each new folder.


Initialize the working environment
------------------------

1. Via the "Git" console, run ./setup

2. Via "Brackets", run "Project > Grunt default" (or "grunt" via the "Git" console)


Making Changes
------------------------

The source files are located in the "src" folder and subfolders. Make your changes there.

To build your changes, run "Project > Grunt default" in Brackets ( or "grunt" via the "Git" console ). Supporting files will be placed in "demos/eic/home.nsf/"

To view your changes in a local environment run "grunt server" in "Git", then navigate to http://127.0.0.1:8080 in your browser.


Updating WET
------------------------

At the time of writing, our "demos" files link against WET v4.0.13.

To use a different WET version, change the 'wet_version="4.0.13"' line in "setup" to another version (ex: wet_version="4.0.14")

And run ./setup via the "Git" console.


Further Reading
------------------------

More information on the build process can be found in "build/README.md"


EPIC integration:
------------------------
http://wiki.ic.gc.ca/display/MOD/WET+4.0
