<div id="phonecontent" class="div1 boxHthin icBgI paddingLeft12 paddingRight12 paddingTop2 paddingBottom2 marginTop8" style="margin-left:-30px;">
            <p>
                <label for="phonecontrol2">What can we help you with?</label>
                <br />
                <select id="phonecontrol2" class="eic-toggle">
                    <option value="1" selected="selected">Select...</option>
                    <option value="2">Bankruptcy</option>
                    <option value="3">Consumer Issues</option>
                    <option value="4">Copyright and Intellectual Property</option>
                    <option value="5">Corporations</option>
                    <option value="6">Funding</option>
                    <option value="7">Import, Export and Investment</option>
                    <option value="8">Industries and Business</option>
                    <option value="9">Internet, radio and wireless</option>
                    <option value="10">Science and Technology</option>
                    <option value="11">Weights and Measurements</option>
                    <option value="12">General Enquiry</option>
                </select>
            </p>
            <div class="phonecontrol2-2">
                <p>
                    <label for="phonecontrol">More specifically...</label>
                    <br />
                    <select name="phonecontrol" id="phonecontrol" class="eic-toggle">
                        <option value="1" selected="selected">Select...</option>
                        <option value="2">General Enquiry</option>
                        <option value="3">Find individuals or businesses that have declared bankruptcy or filed a proposal</option>
                        <option value="4">Find businesses that have filed under Companies' Creditors Arrangement Act (CCAA)</option>
                        <option value="5">For questions about the Companies' Creditors Arrangement Act (CCAA)</option>
                    </select>
                </p>

                <div class="phonecontrol-2">
                   <p><strong>Toll-Free (Canada): 1-877-376-9902</strong><br />
                    Business Hours: 8:30 a.m. to 4:30 p.m.</p>
                </div>

                <div class="phonecontrol-3 phonecontrol-4">
                    <p><strong>Toll-Free: 1-866-941-2863</strong><br />
                    Fax: 1-877-827-7139<br />
                    Business Hours: 8:30 a.m. to 4:30 p.m.</p>
                </div>

                <div class="phonecontrol-5">
                    <p><strong>Toll-Free: 1-866-376-9902</strong><br />
                    Business Hours: 8:30 a.m. to 4:30 p.m.</p>
                </div>

            </div>
          <div class="phonecontrol2-4">
                <p>
                    <label for="phonecontrol8">More specifically...</label>
                    <br />
                    <select name="phonecontrol8" id="phonecontrol8" class="eic-toggle">
                        <option value="1" selected="selected">Select...</option>
                        <option value="2">General Enquiry</option>
                        <option value="3">Copyrights</option>
                        <option value="4">Industrial Designs</option>
                        <option value="5">Integrated Circuit Topographies</option>
                        <option value="6">Patents</option>
                        <option value="7">Patent Appeal Board</option>
                        <option value="8">Trade-marks</option>
                        <option value="9">Trade-marks Opposition Board</option>
                        <option value="10">Executive Office</option>
                        <option value="11">Finance</option>
                        <option value="12">Intellectual Property Outreach Program</option>
                        <option value="13">Media Relations</option>
                    </select>
                </p>

                <div class="phonecontrol8-2">
                   <p><strong>CIPO Client Service Centre</strong><br />
                    Toll-free (Canada and United States): 1-866-997-1936 <br />
                    International Calls Only: 1-819-934-0544<br />
                    TTY: 1-866-442-2476<br />
                    Fax: 1-819-953-CIPO (2476)<br />
                    </p>
                </div>

                <div class="phonecontrol8-3 phonecontrol8-4">
                    <p><strong>Toll-Free: 1-866-941-2863</strong><br />
                    Fax: 1-877-827-7139<br />
                    Business Hours: 8:30 a.m. to 4:30 p.m.</p>
                </div>

                <div class="phonecontrol8-5">
                    <p><strong>Toll-Free: 1-866-376-9902</strong><br />
                    Business Hours: 8:30 a.m. to 4:30 p.m.</p>
                </div>

            </div>
           </div>
