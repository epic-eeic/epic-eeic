<div id="visitcontent" class="div1 equalize boxHthin icBgI padding5" style="margin-left:-30px;">
            <div class="span-1 eic-row-start"></div>
            <div class="span-2"> <br />
            <ul>

<li><a href="#HQ">Headquarters</a></li>

<li><a href="#BC">British Columbia</a></li>

<li><a href="#AB">Alberta</a></li>

<li><a href="#SK">Saskatchewan</a></li>

<li><a href="#MB">Manitoba</a></li>

<li><a href="#ON">Ontario</a></li>

<li><a href="#QC">Quebec</a></li>

<li><a href="#NL">Newfoundland and Labrador</a></li>

<li><a href="#PEI">Prince Edward Island</a></li>

<li><a href="#NB">New Brunswick</a></li>

<li><a href="#NS">Nova Scotia</a></li>

<li><a href="#YK">Yukon</a></li>

<li><a href="#NWT">Northwest Territories</a></li>

<li><a href="#NU">Nunavut</a></li>

</ul>

            
</div>
            <div class="span-3 eic-row-end">



               
            </div>
            <div class="clear"></div>

            <h2 id="HQ">Headquarters</h2>

            <p>Industry Canada<br />
            C.D. Howe Building<br />
            235 Queen Street<br />
            Ottawa, Ontario&nbsp;&nbsp;K1A 0H5<br />
            Canada<br />
            Telephone: 613-954-5031<br />
            Toll Free: 1-800-328-6189 (Canada)<br />
            Toll Free Hearing Impaired only <abbr title="Teletypewriter">TTY</abbr>:<br />
            1-866-694-8389<br />
            Fax: 613-954-2340</p><br />


            <h2 id="BC">British Columbia</h2>

            <p><strong>Industry Canada</strong><br />
            Suite&nbsp;2000<br />
            300&nbsp;West Georgia Street<br />
            Vancouver, British Columbia&nbsp;&nbsp;V6B&nbsp;6E1<br />
            Canada<br />
            Telephone: 604-666-5000<br />
            Fax: 604-666-8330</p>


            <h2 id="AB">Alberta</h2>

            <p><strong>Industry Canada</strong><br />
            Suite&nbsp;725<br />
            9700&nbsp;Jasper Avenue<br />
            Edmonton, Alberta&nbsp;&nbsp;T5J&nbsp;4C3<br />
            Canada<br />
            Telephone: 780-495-4782<br />
            Toll-Free: 1-800-461-2646<br />
            Fax: 780-495-4507</p>

            <p><strong>Industry Canada</strong><br />
            Suite&nbsp;400<br />
            639&nbsp;- 5th&nbsp;Avenue <abbr title="South West">S.W.</abbr><br />
            Calgary, Alberta&nbsp;&nbsp;T2P&nbsp;0M9<br />
            Canada<br />
            <abbr title="Telephone">Telephone</abbr>: 403-292-4575<br />
            Fax: 403-292-4295</p>


            <h2 id="SK">Saskatchewan</h2>

            <p><strong>Industry Canada</strong><br />
            7th Floor<br />
            123 Second Avenue South<br />
            Saskatoon, Saskatchewan&nbsp;&nbsp;S7K 7E6<br />
            Canada<br />
            Telephone: 306-975-4400<br />
            Fax: 306-975-4231</p>

            <p><strong>Industry Canada</strong><br />
            Suite&nbsp;600<br />
            1945&nbsp;Hamilton Street<br />
            Regina, Saskatchewan&nbsp;&nbsp;S4P&nbsp;2C7<br />
            Canada<br />
            Telephone: 306-780-5010<br />
            Fax: 306-780-6506</p>


            <h2 id="MB">Manitoba</h2>

            <p><strong>Industry Canada</strong><br />
            4th Floor<br />
            400 <abbr title="Saint">St.</abbr>&nbsp;Mary Avenue<br />
            Winnipeg, Manitoba&nbsp;&nbsp;R3C 4K5<br />
            Canada<br />
            Telephone: 204-983-5851<br />
            Fax: 204-983-3182</p>


            <h2 id="ON">Ontario</h2>

            <p><strong>Industry Canada</strong><br />
            4th Floor<br />
            151 Yonge Street<br />
            Toronto, Ontario&nbsp;&nbsp;M5C 2W7<br />
            Canada<br />
            Telephone: 416-973-5000<br />
            Fax: 416-973-8714</p>


            <h2 id="QC">Quebec</h2>

            <p><strong>Industry Canada</strong><br />
            7th Floor<br />
            5 <span lang="fr" xml:lang="fr">Place Ville-Marie</span><br />
            Montreal, Quebec&nbsp;&nbsp;H3B 2G2<br />
            Canada<br />
            Telephone: 514-496-1797<br />
            Toll-free: 1-888-237-3037<br />
            Fax: 514-283-2247</p>


            <h2 id="NL">Newfoundland and Labrador</h2>

            <p><strong>Industry Canada</strong><br />
            10th Floor<br />
            John Cabot Building<br />
            10 Barter's Hill<br />
            <abbr title="Post Office">P.O.</abbr> Box&nbsp;8950<br />
            <abbr title="Saint">St.</abbr>&nbsp;John's, Newfoundland and Labrador&nbsp;&nbsp;A1B 3R9<br />
            Canada<br />
            Telephone: 709-772-4866<br />
            Fax: 709-772-5093</p>


            <h2 id="PEI">Prince Edward Island</h2>

            <p><strong>Industry Canada</strong><br />
            2nd Floor<br />
            191 University Avenue<br />
            Charlottetown, Prince Edward Island&nbsp;&nbsp;C1A 4L2<br />
            Canada<br />
            Telephone: 902-626-2975<br />
            Fax: 902-566-6859</p>



            <h2 id="NB">New Brunswick</h2>

            <p><strong>Industry Canada</strong><br />
            4th Floor, Unit&nbsp;103<br />
            1045 Main Street<br />
            Moncton, New Brunswick&nbsp;&nbsp;E1C 1H1<br />
            Canada<br />
            Telephone: 506-851-6521<br />
            Fax: 506-851-6429</p>


            <h2 id="NS">Nova Scotia</h2>

            <p><strong>Industry Canada</strong><br />
            Suite 1605<br />
            1505 Barrington Street<br />
            <abbr title="Post Office">P.O.</abbr> Box&nbsp;940, Station&nbsp;&quot;M&quot;<br />
            Halifax, Nova Scotia&nbsp;&nbsp;B3J 2V9<br />
            Canada<br />
            Telephone: 902-426-3459<br />
            Fax: 902-426-2615</p>


            <h2 id="YK">Yukon</h2>

            <p><strong>Industry Canada</strong><br />
            Suite&nbsp;205<br />
            300&nbsp;Main Street<br />
            Whitehorse, Yukon&nbsp;&nbsp;Y1A&nbsp;2B5<br />
            Canada<br />
            Telephone: 867-667-5102<br />
            Fax: 867-393-6711</p>


            <h2 id="NWT">Northwest Territories</h2>

            <p><strong>Industry Canada</strong><br />
            3rd&nbsp;Floor<br />
            <abbr title="Postal Office">P.O</abbr> Box 1735<br />
            5101&nbsp;- 50th&nbsp;Avenue<br />
            Yellowknife, Northwest Territories&nbsp;&nbsp;X1A&nbsp;2P3<br />
            Canada<br />
            Telephone: 867-766-8422<br />
            Fax: 867-766-8426</p>


            <h2 id="NU">Nunavut</h2>

            <p><strong>Industry Canada</strong><br />
            Qimugjuk Building, Building&nbsp;969<br />
            <abbr title="Postal Office">P.O</abbr> Box&nbsp;1750<br />
            Iqaluit, Nunavut&nbsp;&nbsp;X0A&nbsp;0H0<br />
            Canada<br />
            Telephone: 867-975-4669<br />
            Fax: 867-975-4670</p>
        </div>
