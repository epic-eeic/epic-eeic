<div class="mrgn-tp-lg wb-frmvld">
<form method="post" action="../eng/go" class="eicForm" id="_SRT" role="form">
    <div>
	    <div class="form-group"> 
        <label for="csbfp" class="csbfp required"><b><span class="field-name">How did you hear about this Program?</span></b></label>
        <select required class="full-width form-control csbfp" name="csbfp" size="1" id="csbfp">
            <option value="" >Select...</option>
            <option value="Internet search" >Internet search</option>
            <option value="Received information by mail" >Received information by mail</option>
            <option value="Word of mouth" >Word of mouth</option>
            <option value="Business support organisation" >Business support organisation</option>
            <option value="Financial institution" >Financial institution</option>
            <option value="Service Canada" >Service Canada</option>
            <option value="Other" >Other</option>
        </select>
		</div>
		
        <div class="form-group">
          <label class="required" for="Subject"><span class="field-name">Subject</span></label>
          <input required type="text" name="Subject" id="Subject" size="60" class="full-width form-control" pattern="[A-Za-z0-9\s]{4,}" data-rule-minlength="4" />
		</div>

        <div class="form-group">
		  <label class="ic-q required" for="Body">Your question or comment: <a class="ic-priv pull-right" href="07139.html">Privacy Notice</a></label>
          <textarea required name="Body" id="Body" cols="60" rows="8" class="full-width form-control"></textarea>
		</div>

        <p>
          <input class="btn btn-info btn-xs" id="attachment" type="button" value="Attachments" />
          <span id="n039"></span>
        </p>
		
		<fieldset class="hidefieldset">
				<legend class="mrgn-tp-md mrgn-bttm-0">Do you need a response?</legend>
				<div class="radio mrgn-bttm-lg">
					<label class="ic-a-yes radio-inline" for="net_question_0">
					<input type="radio" name="net_question" id="net_question_0" value="Yes" />Yes</label>
					<label class="ic-a-no radio-inline" for="net_question_1">
					<input type="radio" name="net_question" id="net_question_1" value="No" checked="checked" />No</label>
				</div>
		</fieldset>

        <div id="contact-info-box">
        <h3 class="mrgn-tp-0">Contact Information</h3>
        <p>Mandatory fields are indicated by an asterisk (<span class="text-danger">*</span>)<p>
        <div class="form-group">
            <label for="net_Email" class="required"><span class="field-name">Email Address:</span></label>
            <input required type="email" name="net_Email" id="net_Email" size="30" class="required email form-control" />
        </div>

        <div class="form-group">
            <label for="net_FirstName" class="required"><span class="field-name">First Name:</span></label>
            <input required type="text" name="net_FirstName" id="net_FirstName" size="30" class="form-control" />
        </div>

        <div class="form-group">
            <label for="net_LastName" class="required"><span class="field-name">Last Name:</span></label>
            <input required type="text" name="net_LastName" id="net_LastName" size="30" class="form-control" />
        </div>

        <div class="form-group mrgn-tp-md mrgn-bttm-lg">
            <input type="checkbox" id="net_international" name="net_international" style="margin-bottom: 15px;">&nbsp;<label style="display: inline;" for="net_international"><span class="field-name">International Client?</span></label>

            <span class="national_Phone">
                <label for="net_Phone" class="required"><span class="field-name">Telephone:</span> <span class="small text-muted"><i>(999) 999-9999</i></span></label>
                (<input required type="tel" class="form-control area required" style="display:inline; width:15%;" title="Area Code" name="net_PhoneArea" id="net_PhoneArea" size="3" maxlength="3" />) <input required type="tel" title="Number" name="net_Phone" id="net_Phone" size="8" maxlength="8" class="phone form-control" style="display:inline;" />

                <label style="display:inline;" title="Extension" for="net_Ext">Ext.:</label>
                <input style="display:inline;" type="text" class="form-control" name="net_Ext" id="net_Ext" size="5" />
            </span>

            <div class="national_Fax mrgn-tp-md">
                <label for="net_Fax">Fax: <span class="small text-muted"><i>(999) 999-9999</i></span></label>
                (<input type="tel" title="Area code" class="form-control" name="net_FaxArea" id="net_FaxArea" size="3" maxlength="3" style="display:inline; width:15%;" />)
                <input type="tel" title="Number" class="form-control" style="display:inline;" name="net_Fax" id="net_Fax" size="8" maxlength="8" />
            </div>

            <div class="international_Phone">
                <label for="net_international_Phone"><span class="field-name">Telephone:</span></label>
                <input class="form-control" maxlength="20" id="net_international_Phone" name="net_international_Phone">
            </div>

            <div class="international_Fax mrgn-tp-md">
                <label for="net_international_Fax">Fax:</label>
                <input class="form-control" style="display:inline;" name="net_international_Fax" id="net_international_Fax" maxlength="20" />
            </div>
        </div>

    </div>
	<p><input class="btn btn-primary" type="submit" value="Send" /></p>
    <input type="hidden" name="to" id="to" value="info@ic.gc.ca" />
    <input type="hidden" name="hid_select_email_main" id="hid_select_email_main" value="" />
    <input type="hidden" name="hid_sub_email" id="hid_sub_email" value="" />
    <input type="hidden" name="hid_subject_email" id="hid_subject_email" value="" />
    <input type="hidden" name="hid_sub_subject_email" id="hid_sub_subject_email" value="" />
    <input type="hidden" name="hid_pageid" id="hid_pageid" value="n/a" />
    <input type="hidden" name="hid_referrer" id="hid_referrer" value="" />
    <input type="hidden" name="lang" id="lang" value="eng" />
    <input type="hidden" name="presubject" id="lang" value="IC Web Enquiry" />
    <input type="hidden" name="$id" value="YDES-8VAL9G" />
    <input type="hidden" name="$sid" value="PSNS-8VANZZ" />
    <input type="hidden" name="$pg" value="1" />
    <input type="hidden" name="$referer" value="http://www.ic.gc.ca/eic/site/icgc.nsf/frm-eng/YDES-8VAL9G" />
</form>
