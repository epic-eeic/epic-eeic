The "db-assets" folder
========================

In this folder, you can add assets (attachments, images) that live in an EPIC db (i.e.: are unrelated to the templates) but are necessary for a demo page to function locally.

The folder structure is as follows:

* Attachments: "databaseID.nsf/vwapj/file.txt"
* Images: "databaseID.nsf/vwimages/file.png"

One example where this is used is for IC's "Contact Us" page.  
The JavaScript fetches XML files from icgc.nsf to populate the dropdown menus but because JavaScript can't make cross-domain requests by default, the local environment (127.0.0.1) can't fetch files from "ic.gc.ca" or "stratpre1.ic.gc.ca".

To get around this, a copy of the XML files are located in "db-assets/icgc.nsf/vwapj/".  
Grunt will take care of copying those files in the "demos" folder and generate the proper paths ("filename.txt/$file/filename.txt")
