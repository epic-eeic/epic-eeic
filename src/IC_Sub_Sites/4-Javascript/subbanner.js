/**
 * IC Subbanner
 *
 * Note: the EPIC generated subbaner has an additional .eic-subbanner class
 *  on it. .eic-subbanner doesn't have any styles associated with it;
 *  it is a way to target (e.g.: hide) the generated subbanner but not other
 *  ".ic-subbanner"s that might appear on the page.
 *
 * HTML:
 *  <p class="ic-subbanner h1">About us</p>
 *
 * SCSS:
 *  subbanner.scss
 *
 * Refs:
 *  EPI-10538
 */

( function() {
    "use strict";

    var $icSubbanner = $( "main .ic-subbanner" ).first(),
        $eicSubbanner = $( ".eic-subbanner" ).first();

    if ( $icSubbanner.length > 0 ) {
        $eicSubbanner.html( $icSubbanner.html() )
            .show();
    }
}() );
