/**
 * Downward arrow on active WET toggle
 *
 * We also smooth-scroll to the appropriate content
 *
 * SCSS:
 *  tabs.scss
 *
 * Refs:
 *  EPI-10527
 */

( function() {
    /*global eic*/
    "use strict";

    var $tabs = $( ".ic-tabs .wb-toggle, .ic-mobile-tabs .wb-toggle" ),
        hash = window.location.hash,
        changeTabs,
        scroll;

    scroll = function( e, $tab ) {
        var container = $tab.data( "toggle" ).selector;

        if ( $tab.parent().hasClass( "ic-tabs" ) ) {
            eic.smoothScroll( e, "#ic-sub-menu", container, container + "-" );
        } else {
            eic.smoothScroll( e, container, container, container + "-" );
        }
    };

    changeTabs = function() {
        var hash = window.location.hash.slice( 0, -1 );

        $tabs.filter( ":visible" ).each( function() {
            var $tab = $( this );

            if ( $tab.data( "toggle" ).selector === hash ) {
                $tab.trigger( "click" );
            }
        } );
    };

    $tabs
        .on( "click", function() {
            var $tab = $( this );

            $tabs.removeClass( "ic-tab--active" );
            $tab.addClass( "ic-tab--active" );
        } )
        .one( "click", function() {
            $( "#ic-default-toggle" ).remove();
        } )
        .on( "ic-ajax-done", function( e ) {
            scroll( e, $( this ) );
        } );

    $( document ).on( "toggle.wb-toggle", function( e ) {
        var $tab = $( e.target );

        if ( $tab.closest( ".ic-mobile-tabs, ic.tabs" ).length > 1 ) {
            scroll( e, $tab );
        }
    } );

    if ( hash !== "" ) {
        if ( wb.isReady ) {
            changeTabs();
        } else {
            $( document ).one( "wb-ready.wb", changeTabs );
        }
    }
}() );
