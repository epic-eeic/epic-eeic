/**
 * Utility functions to manipulate URL hashes
 *
 * For various reasons, we sometimes use the URL hash as a key/value
 * store (like a query string). These functions aim to make it easier
 * to extract/edit those keys/values.
 *
 * Required by:
 *  page-id.js
 */

( function() {
    "use strict";

    var eic = window.eic || {};

    eic.url = {

        /**
         * Add new parameters to a hash
         * @param   {jQuery|DOM|String}   target    elements to update
         * @param   {Object} newParams new parameters
         * @returns {jQuery} elements that have been updated
         */
        setHashParams: function( target, newParams ) {
            var $elm = ( target.jquery ) ? target : $( target );

            $elm.each( function() {
                var $this = $( this ),
                    elm = $this.get( 0 ),
                    params = eic.url.getHashParams( $this ),
                    hash = "",
                    href = $this.attr( "href" );

                // overwrite/add newParams data to link's current params
                $.each( newParams, function( key, val ) {
                    params[ key ] = val;
                } );

                // object to string
                $.each( params, function( key, val ) {
                    hash += "&" + key + "=" + val;
                } );

                // replace leading "&" with "#"
                hash = hash.replace( /^&/, "#" );

                // update href value
                if ( elm.hash === "" ) {
                    href += hash;
                } else {
                    href = href.replace( elm.hash, hash );
                }

                // update the element
                $this.attr( "href", href );
            } );

            // chaining
            return $elm;
        },

        /**
         * Get the keys/values in the hash of a targetted element
         *
         * Based on WET's wb.getUrlParts, but uses <url>.hash instead of <url>.search
         *
         * @param   {jQuery|DOM|String}   target    element to get params from
         * @returns {Object} key/value map of the hash parameters
         */
        getHashParams: function( target ) {
            var elm = ( target.jquery ) ? target.get( 0 ) : target,
                keypairs = elm.hash.replace( /^#/, "" ).split( "&" ),
                len = keypairs.length,
                ret = {},
                keypair,
                i;

            for ( i = 0; i < len; i += 1 ) {
                if ( ( keypair = keypairs[ i ] ) !== null ) {
                    keypair = keypair.split( "=" );
                    ret[ keypair[ 0 ] ] = keypair[ 1 ];
                }
            }

            return ret;
        }
    };

    window.eic = eic;
}() );
