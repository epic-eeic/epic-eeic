/**
 * IC Dismissible
 *
 * Dismissible containers have a "close" button that makes the container disappear
 * and sets a cookie so it stays invisible when re-visiting the page.
 *
 * data-* attributes are optional and default to:
 *  - data-cname: "outage"
 *  - data-expires: next Wednesday
 *
 * HTML:
 *  <div class="js-ic-dismissible" data-cname="cookie-name" data-expires="1970-01-01">
 *    Content
 *  </div>
 *
 * SCSS:
 *  dismissible.scss
 *
 * Refs:
 *  ICGCWEB-160
 *  EPI-10320
 *  EPI-10705
 */

( function( window ) {
    "use strict";

    var $containers = $( ".js-ic-dismissible" ),
        lang = $( "html" ).attr( "lang" ),
        eic = window.eic,
        close = ( lang === "en" ) ? "close" : "fermer",
        closeThisBox = ( lang === "en" ) ? "Close this box" : "Fermer cette boîte";

    $containers.each( function() {
        var $container = $( this ),
            cookieName = $container.data( "cname" ) || "outage";

        if ( eic.getCookie( cookieName ) !== "dismissed" ) {
            $( "<button button type='button' title='" + close + "' class='glyphicon glyphicon-remove-circle ic-close'><span class='wb-inv'>" + closeThisBox + "</span></button>" )
                .appendTo( $container )
                .on( "click", function() {
                    var expires = $container.data( "expires" ),
                        date = new Date();

                    if ( expires === undefined ) {
                        expires = 3 - date.getDay(); // Next wednesday

                        if ( expires <= 0 ) {
                            expires = expires + 7;
                        }
                    }

                    eic.setCookie( cookieName, "dismissed", expires );

                    $container.slideUp();
                } );

            $container.css( "display", "block" );
        }
    } );
}( window ) );
