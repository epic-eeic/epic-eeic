/**
 * Animate details/summary element/polyfill
 *
 * HTML:
 *  <details class="ic-details well">
 *      <summary>Test</summary>
 *      <div>
 *          <p>Content</p>
 *      </div>
 *  </details>
 *
 * SCSS:
 *  details.scss
 *
 * Refs:
 *  EPI-10332
 */

( function() {
    "use strict";

    if ( wb.ielt9 ) {
        return;
    }

    $( ".ic-details details[open]" ).children( "div" ).first().show();

    $( document ).on( "click", "html.details .ic-details summary", function( e ) {
        var isNative = $( "html" ).hasClass( "details" ),
            $summary = $( this ),
            $details = $summary.parent(),
            $wrapper = $details.children( "div" ).first();

        e.preventDefault();

        if ( $details.attr( "open" ) ) {
            $wrapper.slideUp( function() {
                if ( isNative ) {
                    $details.removeAttr( "open" );
                }
            } );
        } else {
            if ( isNative ) {
                $details.attr( "open", "open" );
            }

            $wrapper.slideDown();
        }
    } );
}() );
