/**
 * JS-enabled form "action"
 *
 * Use a different "action" page if JS is enabled.
 * Fallback to original value if not.
 *
 * HTML:
 *  <form data-icAction="/eic/site/ui-icgc.nsf/eng/06957.html" action="http://www.google.com/cse">
 *      [...]
 *  </form>
 *
 * Refs:
 *  EPI-10532
 */

( function() {
    "use strict";

    $( "[data-icAction]" ).each( function() {
        var $this = $( this );

        $this.attr( "action", $this.data( "icaction" ) );
    } );

}() );
