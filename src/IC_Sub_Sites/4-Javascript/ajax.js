/**
 * IC ajax tabs
 *
 * When JS is enabled, clicking on a <a data-icAjax=""> link will fetch the content
 * from "url" and place it into "container" instead of changing pages. When JS is
 * disabled, links fallback to their default behaviour and change page to "href".
 *
 * HTML:
 *  <div class="ic-btn-group-justified-min-height btn-group btn-group-justified ic-eqht-mdl hidden-xs">
 *      <a data-icAjax='{"url": "/eic/library.nsf/eng/WDES-9QHVPS", "container": "#you-owe-money"}'
 *         href="06932.html#ic-sub-menu" class="btn btn-info pdng-menu-2-lg">You owe money</a>
 *      <a data-icAjax='{"url": "/eic/library.nsf/eng/WDES-9QHVNE", "container": "#you-are-owed-money"}'
 *         href="06933.html#ic-sub-menu" class="btn btn-info pdng-menu-lg">You are owed money</a>
 *      <a data-icAjax='{"url": "/eic/library.nsf/eng/WDES-9QHNGS", "container": "#for-researchers"}'
 *         href="06934.html#ic-sub-menu" class="btn btn-info pdng-menu-2-lg">For researchers</a>
 *      <a data-icAjax='{"url": "/eic/library.nsf/eng/WDES-9QHVJG", "container": "#for-journalists"}'
 *         href="07045.html#ic-sub-menu" class="btn btn-info pdng-menu-lg">For journalists</a>
 *      <a data-icAjax='{"url": "/eic/library.nsf/eng/WDES-9QHVLV", "container": "#for-trustees"}'
 *         href="07046.html#ic-sub-menu" class="btn btn-info pdng-menu-lg">For trustees</a>
 *  </div>
 *
 * Refs:
 *  EPI-10527
 */

( function() {
    "use strict";

    var $ajaxes = $( "[data-icAjax]" );

    $ajaxes.each( function() {
        var $ajax = $( this ),
            data = $ajax.data( "icajax" ),
            url = data.url,
            $container = $( data.container );

        $ajax.one( "click", function( e ) {
            if ( !$container.hasClass( "ic-ajax-loaded" ) ) {
                e.preventDefault();

                $container.addClass( "ic-ajax-loaded" );
                $container.load( url, function() {
                    wb.add( data.container + " [class*='wb-']" );

                    $( data.container + " .ic-show-menu" ).trigger( "change" );
                    $ajax.trigger( "ic-ajax-done" );
                } );
            }
        } );
    } );
}() );
