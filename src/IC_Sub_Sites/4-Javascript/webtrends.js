/*global dcsMultiTrack: false*/
( function( dcsMultiTrack ) {
    "use strict";

    /**
     * Feedback form
     *
     * Refs:
     *  EPI-10717
     */
    $( ".fdbck-frm [type='submit']" ).on( "click", function() {
        dcsMultiTrack( "WT.ic_eventType", "megaphoneSubmit", "WT.dl", "31" );
    } );

}( dcsMultiTrack ) );
