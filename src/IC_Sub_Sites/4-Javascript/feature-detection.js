/**
 * Feature detection
 *
 * Add classes to <html> depending on if the browser supports certain features
 */

/**
 * Check for <canvas> support
 *
 * Required by:
 *  feedback.scss
 *
 * From:
 *  https://github.com/Modernizr/Modernizr/blob/924c7611c170ef2dc502582e5079507aff61e388/feature-detects/canvas.js
 *
 */

( function() {
    "use strict";

    var $html = $( "html" ),
        cls = "canvas",
        elm = document.createElement( cls );

    cls = ( !!( elm.getContext && elm.getContext( "2d" ) ) ) ? cls : "no-" + cls;

    $html.addClass( cls );
}() );
