/**
 * Feedback panel
 *
 * Panel sitting at the bottom right of the screen in the "Configuration"
 * section of the IC Sub Sites Master template.
 *
 * SCSS:
 *  feedback.scss
 *
 * Refs:
 *  EPI-9354
 */

( function() {
    /*global UAParser, eic*/
    "use strict";

    if ( window.location.hostname !== "127.0.0.1" && window.location.hostname !== "stratdemo.ic.gc.ca" && window.location.hostname !== "www.ic.gc.ca" && window.location.hostname !== "stratpre1.ic.gc.ca" ) {
        $( ".ic-fdbck" ).hide();
        return;
    }

    var $elm = $( ".ic-fdbck" ),
        sid,
        updateSID,
        $frm = $elm.find( "#feedback-form" ),
        parser = new UAParser(), /* sets the os, browser and screen size */
        ua = parser.getResult(),
        processComments,
        showThanks,
        resetForm,
        $error = $elm.find( ".js-ic-error" ),
        $html = $( "html" ),
        lang = $html.attr( "lang" ).substr( 0, 1 ), /* 'e' or 'f' */
        reposition,
        $footer = $( "#wb-info" ),
        $window = $( window ),
        $contentArea = $( "main" ).closest( ".container" );

    // Reposition
    reposition = function() {
        if ( $html.hasClass( "smallview" ) || $html.hasClass( "xsmallview" ) ) {
            return;
        }

        var footerOffsetTop = $footer.offset().top,
            scrollTop = $window.scrollTop();

        if ( ( scrollTop + $window.height() ) < ( footerOffsetTop + 35 ) ) {
            $elm.css( {
                "bottom": "15px",
                "position": "fixed",
                "right": "25px"
            } );
        } else {
            $elm.css( {
                "bottom": "-75px",
                "position": "absolute",
                "right": -( $window.width() - $contentArea.width() ) / 2 + 25 + "px"
            } );
        }
    };

    // SendAsBinary polyfill (https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest)
    if ( !XMLHttpRequest.prototype.sendAsBinary ) {
        XMLHttpRequest.prototype.sendAsBinary = function( sData ) {
            var nBytes = sData.length, nIdx, ui8Data = new window.Uint8Array( nBytes );

            for ( nIdx = 0; nIdx < nBytes; nIdx += 1 ) {
                ui8Data[ nIdx ] = sData.charCodeAt( nIdx ) & 0xff;
            }

            // Send as ArrayBufferView
            this.send( ui8Data );
        };
    }

    // Populate fields with the information we have at this point
    $elm.find( "input[name='osinfo']" ).val( ua.os.name + " " + ( ( ua.os.version === undefined ) ? "" : ua.os.version ) );
    $elm.find( "input[name='browserinfo']" ).val( ua.browser.name + " " + ua.browser.version );
    $elm.find( "input[name='sizeinfo']" ).val( window.screen.width + " x " + window.screen.height );
    $elm.find( "input[name='url']" ).val( window.location );
    $( document ).ready( function() {
        $elm.find( "input[name='pageid']" ).val( $( ".page-id" ).text().replace( /^Page: /, "" ) );
        if ( $( ".ic-phone" ).length > 0 ) {
            $elm.find( ".js-helpnow" ).attr( "href", $( ".ic-phone" ).attr( "href" ) );
        }
    } );

    updateSID = function() {
        $.get( "/eic/site/icgc.nsf/frm-eng/VVIK-9A5SHD", function( data ) {
            var form, fields;

            form = $( data ).find( ".eic-form" );
            fields = form.find( "input[name='$key'], input[name='$sid']" );
            fields.each( function() {
                var $this = $( this ),
                    $attachForm = $( "#attachForm" );

                $frm.find( "[name='" + $this.attr( "name" ) + "']" ).val( $this.val() );

                if ( $this.attr( "name" ) === "$sid" ) {
                    sid = $this.val();
                    $attachForm.find( "[name='$sid']" ).val( sid );
                    $attachForm.find( "[name='$ckey']" ).val( sid + ":VVIK-9A5SHD:n006" );
                }
            } );
        } );
    };

    processComments = function() {

        // POST the form containing comments
        $.post( $frm.attr( "action" ), $frm.serialize() );

        showThanks();
    };

    showThanks = function() {

        // Show "thanks" panel
        $elm.find( ".ic-fdbck-pnl" ).hide();
        $elm.find( "#thanks" ).show();

        // Hide "thanks" panel after 1sec
        window.setTimeout( function() {
            $frm.removeClass( "sticky" )
                .fadeOut( resetForm );
        }, 1000 );
    };

    resetForm = function() {

        // Hide all panels
        $elm.find( ".ic-fdbck-pnl" ).hide();
        $frm.removeClass( "sticky" );

        // Hide loading icon
        $( "#loading" ).hide();

        // Clear inputs
        $frm.find( "textarea" ).val( "" );

        // Uncheck screenshot box
        $( "#screenshot" ).removeAttr( "checked" );

        // Hide error messages
        $error.hide();

        // Put placeholder polyfill values back
        if ( $.fn.placeholder !== undefined ) {
            $elm.find( "[placeholder]" ).placeholder();
        }

        updateSID();

        // Have the first panel in series shown when form appears
        $elm.find( ".js-panel--first" ).show();
    };

    updateSID();

    $frm.on( "submit", function( e ) {
        e.preventDefault();

        var values = [],
            $textareas = $elm.find( "textarea" ),
            $currPanel = $elm.find( ".ic-fdbck-pnl:visible" ),
            $submit = $currPanel.find( "input[type='submit']" ),
            i,
            len,
            allTheSame = true;

        $textareas.each( function() {
            values.push( $.trim( $( this ).val() ) );
        } );

        for ( i = 1, len = values.length; i < len; i += 1 ) {
            if ( values[ 0 ] !== values[ i ] ) {
                allTheSame = false;
                break;
            }
        }

        // If all fields have the same value
        if ( allTheSame ) {
            if ( values[ 0 ] === "" ) { /* If all fields are empty, show err. msg. */
                $error.insertBefore( $submit )
                    .show();

            } else { /* All four fields contain the same data. Fail silently (flagged as spam). */
                showThanks();
            }

            return false;
        }

        if ( $( "#screenshot" ).is( ":checked" ) ) {
            eic.takeScreenshot( document.body, function( canvas ) {
                if ( canvas === null ) {
                    processComments();
                    return;
                }

                // Build POST request
                var screenshot = canvas.toDataURL( "image/jpeg", 0.1 ).replace( /^data:image\/jpeg;base64,/, "" ),
                    boundary = "---------------------------7da24f2e50046",
                    xhr = new XMLHttpRequest(), /* TODO: check for XMLHttpRequest support, fallback to generic attachment form if not supported */
                    body = [ "--" + boundary,
                        "Content-Disposition: form-data; name=\"$sid\"\r\n",
                        sid,
                        boundary,
                        "Content-Disposition: form-data; name=\"$id\"\r\n",
                        "VVIK-9A5SHD",
                        boundary,
                        "Content-Disposition: form-data; name=\"$aid\"\r\n",
                        "n006",
                        boundary,
                        "Content-Disposition: form-data; name=\"$ckey\"\r\n",
                        sid + ":VVIK-9A5SHD:n006",
                        boundary,
                        "Content-Disposition: form-data; name=\"%%File.8525762B006E0CC1.a52539f2a440685d8525742b0067c275.$Body.0.70\"; filename=\"screenshot.jpg\"",
                        "Content-Type: image/png\r\n",
                        window.atob( screenshot ), /* decode base64 */
                        boundary + "--" ].join( "\r\n" );

                xhr.open( "POST", "/eic/registration.nsf/nFileUpload?createdocument&lang=" + lang, true );
                xhr.setRequestHeader( "Content-Type", "multipart/form-data; boundary=" + boundary );

                // Process comments after the screenshot has been successfully submitted
                // TODO: Send other form regardless of xhr.status?
                xhr.onreadystatechange = function() {
                    if ( xhr.readyState === 4 && xhr.status === 200 ) {
                        processComments();
                    }
                };

                // Submit screenshot
                xhr.sendAsBinary( body );
            } );
        } else { /* No screenshot, just process the comments */
            processComments();
        }
    } );

    // Stop following on scroll when we get to the footer area
    $window.on( "scroll", reposition );
    $( document ).on( "txt-rsz.wb win-rsz-width.wb win-rsz-height.wb", reposition );
    reposition();

    // Fade out when we move mouse away, unless the icon was clicked
    $frm.add( $elm.find( ".ic-fdbck-icn" ) ).on( "mouseleave", function() {
        $elm.removeClass( "hover" );

        setTimeout( function() {
            if ( !$frm.hasClass( "sticky" ) && !$elm.hasClass( "hover" ) ) {
                $frm.stop( true, true ).fadeOut();
            }
        }, 500 );
    } );

    $frm.on( "mouseenter", function() {
        $elm.addClass( "hover" );
    } );

    // Hide popup when clicking anywhere outside of it
    $( "body" ).on( "click", function( e ) {
        if ( $frm.hasClass( "sticky" ) && $( e.target ).closest( ".ic-fdbck" ).length === 0 ) {
            $frm.stop( true, true ).fadeOut();
            $frm.removeClass( "sticky" );
        }
    } );

    // ICGCWEB-3258: Close button to hide the feedback feature
    //               Includeing the megaphone icon
    $elm.find( ".js-fdbck-x-btn" ).on( "click", function( e ) {
        e.preventDefault();

        $elm.hide();
    } );

    // Toggle popup visibility when clicking on the "speaker" icon
    $elm.find( ".ic-fdbck-icn" )
        .on( "click", function( e ) {
            e.preventDefault();

            if ( $frm.hasClass( "sticky" ) ) {
                $frm.stop( true, true ).fadeOut();
            }

            if ( !$frm.is( ":visible" ) ) {
                $frm.stop( true, true ).fadeIn();
            }

            $frm.toggleClass( "sticky" );
        } )
        .on( "mouseenter", function() {
            $elm.addClass( "hover" );

            if ( !$frm.hasClass( "sticky" ) ) {
                $frm.stop( true, true ).fadeIn();
            }
        } );

    // Change panels when clicking on the different buttons
    $elm.find( ".js-button" ).on( "click", function( e ) {
        e.preventDefault();

        var $this = $( this ),
            $nextPanel = $( $this.attr( "href" ) ),
            classes = $.trim( $this.attr( "class" ) ).split( /\s+/ ),
            len = classes.length,
            i,
            cls;

        // Extract the "type" class on the current button and add it to the "thanks" panel
        // This is for changing the "thanks" panel background depending on what was clicked

        for ( i = 0; i < len; i += 1 ) {
            cls = /^ic-fdbck-btn--(.+)/.exec( classes[ i ] );

            if ( cls !== null ) {
                cls = cls[ 1 ];
                break;
            }
        }

        if ( cls !== null ) {
            $( "#thanks" ).attr( "class", "thanks--" + cls + " ic-fdbck-pnl ic-fdbck-pnl--thnks" );
        }

        $this.closest( ".ic-fdbck-pnl" ).hide();
        $nextPanel.show();
    } );

    // Show "attachment" window when clicking on the paperclip icon (<button>)
    $elm.find( "#attachment" ).on( "click", function() {
        window.open( "/eic/registration.nsf/FULoadWind?openagent&sid=" + sid + "&fid=VVIK-9A5SHD&lang=" + lang + "&aid=n006", "newWin", "menubar=no,scrollbars=yes,status=yes,resizable=yes,width=350,height=350" );
    } );
}() );
