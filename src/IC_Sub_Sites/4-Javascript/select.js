/**
 * <select> that directs visitors to another page on submit
 *
 * HTML:
 *  <form class="ic-jump-menu">
 *    <select>
 *      <option value="http://example.org">Option 1</option>
 *      <option value="http://example.com">Option 2</option>
 *    </select>
 *    <input type="submit">
 *  </form>
 *  <noscript>
 *    <ul>
 *      <li><a href="http://example.org">Option1</a></li>
 *      <li><a href="http://example.org">Option1</a></li>
 *    </ul>
 *  </noscript>
 *
 * SCSS:
 *  select.scss
 *
 * Refs:
 *  EPI-10676
 */

( function() {
    "use strict";

    $( document ).on( "submit", ".ic-jump-menu", function( e ) {
        e.preventDefault();

        var $form = $( this ),
            url = $form.find( "option:selected" ).val();

        window.location = url;
    } );
}() );

/**
 * <select> that shows/hides content
 *
 * HTML:
 *  <select id="consumercontrol" class="ic-show-menu">
 *    <option selected="selected">Select</option>
 *    <option>Option 1</option>
 *    <option>Option 2</option>
 *  </select>
 *
 *  <div class="consumercontrol-2">
 *    <p>Content for "Option 1"</p>
 *  </div>
 *
 *  <div class="consumercontrol-3">
 *    <p>Content for "Option 2"</p>
 *  </div>
 *
 * SCSS:
 *  select.scss
 *
 * Refs:
 *  EPI-10676
 */

( function() {
    "use strict";

    $( document ).on( "change", ".ic-show-menu", function() {
        var $select = $( this ),
            $options = $select.children( "option" ),
            id = $select.attr( "id" ),
            index1;

        // Hide containers that aren't targeted by <select>
        $options.filter( ":not(:selected)" ).each( function() {
            index1 = $( this ).index() + 1;

            $( "." + id + "-" + index1 ).hide();
        } );

        // Show containers that are targeted by <select>
        $options.filter( ":selected" ).each( function() {
            index1 = $( this ).index() + 1;

            $( "." + id + "-" + index1 ).fadeIn( function() {
                var $container = $( this );

                // Process nested 'toggle' dropdowns, only if visible
                $container.find( ".ic-show-menu" ).filter( ":visible" ).each( function() {
                    var $nestedSelect = $( this );

                    $nestedSelect.trigger( "change" );
                } );
            } );
        } );
    } );

    // Trigger
    $( ".ic-show-menu" ).trigger( "change" );
}() );
