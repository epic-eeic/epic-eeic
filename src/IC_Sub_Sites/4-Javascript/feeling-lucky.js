/**
 * "Feeling lucky" search feature
 *
 * Given a dictionary of terms/urls pairs, if a search query matches
 * a `term`, redirect browser to `url` instead of showing search
 * results.
 *
 * HTML:
 *  <form class="ic-srch-lcky" action="#">
 *      [...]
 *  </form>
 *
 * Refs:
 *  EPI-10711
 *  /eic/site/icgc.nsf/vwapj/feeling-lucky-eng-v4.js/$file/feeling-lucky-eng-v4.js
 */

/*global eic: false*/
( function() {
    "use strict";

    $( document ).on( "submit", ".ic-srch-lcky", function() {

        // There are no "feeling lucky" terms configured. Bail.
        if ( eic === undefined || eic.search.lucky === undefined ) {
            return;
        }

        var $this = $( this ),
            query = $this.find( "[name='q']" ).val().toLowerCase(),
            lucky = eic.search.lucky;

        if ( lucky[ query ] ) {
            window.location = lucky[ query ];
            return false;
        }
    } );
}() );
