/**
 * Adds an overlay over container on hover/focus
 *
 * HTML:
 *  <div class="ic-overlay">Some content</div>
 *  <div class="ic-overlay ic-overlay--dark">Some content</div>
 *
 * SCSS:
 *   overlays.scss
 *
 * Refs:
 *  EPI-9682
 *  EPI-10330
 *  EPI-10516
 */

( function() {
    "use strict";

    // Wait until images are loaded
    $( document ).one( "wb-ready.wb", function() {
        $( ".ic-overlay" ).each( function() {
            var $this = $( this ),
                $overlay = $( "<span class='js-ic-overlay'></span>" );

            $overlay.appendTo( $this )
                .css( {
                    width: $this.outerWidth(),
                    height: $this.outerHeight()
                } );
        } );

        // Handle text and window resizing
        $( document ).on( "txt-rsz.wb win-rsz-width.wb win-rsz-height.wb", function() {
            $( ".js-ic-overlay" ).each( function() {
                var $this = $( this ),
                    $overlay = $this.closest( ".ic-overlay" );

                $this.css( {
                    width: $overlay.outerWidth(),
                    height: $overlay.outerHeight()
                } );
            } );
        } );
    } );
}() );
