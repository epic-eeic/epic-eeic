/**
 * Screenshot utility function
 *
 * Take a screenshot of the current page (or part of it) by calling
 * eic.takeScreenshot(DOMElement, callback)
 *
 * Requires:
 *  html2canvas ( https://github.com/niklasvh/html2canvas )
 *
 * Required by:
 *  feedback.js
 */

/*global html2canvas: false*/
( function() {
    "use strict";

    var eic = window.eic || {},
        processScreenshot;

    /**
     * @method takeScreenshot
     * @param {DOMElement} dom The container to take screenshot of (ex: document.body)
     * @param {Function} callback The method to call after the screenshot is taken.
     */

    eic.takeScreenshot = function( dom, callback ) {

        // html2canvas library hasn't been loaded yet
        if ( typeof html2canvas === "undefined" ) {

            // Once it's loaded, process screenshot
            yepnope( [
                {
                    load: "/eic/home.nsf/js/ic_WET_4-0_html2canvas.min.js",
                    complete: function() {
                        processScreenshot( dom, callback );
                    }
                }
            ] );
        } else { /* html2canvas has been previously loaded, process screenshot */
            processScreenshot( dom, callback );
        }
    };

    processScreenshot = function( dom, callback ) {
        if ( !html2canvas ) {
            callback( null );
            return;
        }

        // Build screenshot
        html2canvas( dom, {
            onrendered: callback
        } );
    };

    window.eic = eic;
}() );
