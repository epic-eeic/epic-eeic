/**
 * Smooth scroll to the target. If the target
 * doesn't exist, smooth scroll to the top of
 * the page.
 *
 * HTML:
 *  <a class="js-ic-smoothscroll" href="#bottom">Text</a>
 */

( function() {
    "use strict";

    var eic = window.eic || {};

    /**
     * Generic smooth scroll utility function
     *
     * @param e Event
     * @param target (String|jQuery) jQuery id selector. Ex: "#target"
     */
    eic.smoothScroll = function( e, scrollTarget, focusTarget, hash ) {
        e.preventDefault();

        var $scrollTarget = $( scrollTarget ),
            $focusTarget,
            offset = ( $scrollTarget.length ) ? $scrollTarget.offset().top : 0;

        if ( focusTarget === undefined ) {
            $focusTarget = $scrollTarget;
        } else {
            $focusTarget = $( focusTarget );
        }

        if ( hash === undefined ) {
            hash = $scrollTarget.attr( "id" ) || "";
        }

        $( "html, body" ).stop()
            .animate(
                { "scrollTop": offset },
                900,
                "swing",
                function() {
                    window.location.hash = hash;

                    if ( !$focusTarget.is( ":focusable" ) ) {
                        $focusTarget.attr( "tabindex", "-1" );
                    }

                    $focusTarget.focus();
                }
            );
    };

    /**
     * Apply smooth scroll to .js-ic-smoothscroll elements
     */
    $( document ).on( "click", ".js-ic-smoothscroll", function( e ) {
        var $this = $( this ),
            target = $this.attr( "href" );

        eic.smoothScroll( e, target );
    } );

    window.eic = eic;
}() );
