/**
 * ICGCWEB-421: Contact us page ID
 *
 * Refs:
 *  ICGCWEB-421
 *  EPI-11122
 */

( function() {
    "use strict";

    var $footer = $( "#wb-info" ),
        $footerSections = $footer.find( "section" ),
        contactUs = ( wb.lang === "en" ) ? "contact us" : "contactez-nous",
        pageid,
        $elms = $( "" ),
        eic = window.eic || {};

    // ICGCWEB-421: If we're on the contact us page, take pageid from url if present
    if ( document.getElementById( "contactUs" ) !== null ) {
        pageid = eic.url.getHashParams( window.location.href ).pageid;
    } else { /* Otherwise, take it from .page-id */
        pageid = $footer.find( ".page-id" ).text().replace( /Page\s?: /, "" );
    }

    // Couldn't find pageid, bail.
    if ( !pageid ) {
        return;
    }

    $footerSections.each( function() {
        var $section = $( this ),
            $heading = $section.find( "h3" );

        if ( $heading.text().toLowerCase() === contactUs ) {
            $elms = $heading.find( "a" )
                        .add( $section.find( "li a" ).slice( 0, -1 ) );

            return false;
        }

    } );

    // Add the pageid parameter to the hash params
    eic.url.setHashParams( $elms, { "pageid": pageid } );
}() );
