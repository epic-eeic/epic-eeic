/**
 * Megamenu fixes
 *
 * When JS is enabled, the top level links should be anchors to their <ul>
 * This is required to have keyboard navigation work as intended.
 * EPIC can't to this, so fix it via JS since this is a progressive enhancement
 *
 * HTML (before JS):
 *   <nav role="navigation" id="wb-sm" data-trgt="mb-pnl" class="wb-menu visible-md visible-lg" typeof="SiteNavigationElement">
 *     <div class="container nvbar">
 *       <h2>Topics menu</h2>
 *       <div class="row">
 *         <ul class="list-inline menu">
 *           <li><a href="../../../index-en.html">WET project</a>
 *             <ul class="sm list-unstyled" role="menu">
 *               <li><a href="http://wet-boew.github.io/v4.0-ci/index-en.html#about">About the Web Experience Toolkit</a></li>
 *             </ul>
 *           </li>
 *           <li><a href="../../../docs/start-en.html#implement">Implement WET</a>
 *             <ul class="sm list-unstyled" role="menu">
 *               <li><a href="http://wet-boew.github.io/wet-boew/docs/start-en.html">Getting started with WET</a><</li>
 *             </ul>
 *           </li>
 *           <li><a href="../../../docs/start-en.html">Contribute to WET</a>
 *             <ul class="sm list-unstyled" role="menu">
 *               <li><a href="http://wet-boew.github.io/wet-boew/docs/bugs-en.html">Filing a bug or an issue</a></li>
 *             </ul>
 *           </li>
 *         </ul>
 *       </div>
 *     </div>
 *   </nav>
 *
 * HTML (after JS):
 *   <nav role="navigation" id="wb-sm" data-trgt="mb-pnl" class="wb-menu visible-md visible-lg" typeof="SiteNavigationElement">
 *     <div class="container nvbar">
 *       <h2>Topics menu</h2>
 *       <div class="row">
 *         <ul class="list-inline menu">
 *           <li><a href="#1ef287f9-41a4-47da-a5ff-69f290f2d22d">WET project</a>
 *             <ul class="sm list-unstyled" id="1ef287f9-41a4-47da-a5ff-69f290f2d22d" role="menu">
 *               <li><a href="http://wet-boew.github.io/v4.0-ci/index-en.html#about">About the Web Experience Toolkit</a></li>
 *             </ul>
 *           </li>
 *           <li><a href="#c1da606a-a4eb-439f-aa24-d307235b6d6e">Implement WET</a>
 *             <ul class="sm list-unstyled" id="c1da606a-a4eb-439f-aa24-d307235b6d6e" role="menu">
 *               <li><a href="http://wet-boew.github.io/wet-boew/docs/start-en.html">Getting started with WET</a><</li>
 *             </ul>
 *           </li>
 *           <li><a href="#683a7b24-9000-4c8c-b885-4ca1bc195f62">Contribute to WET</a>
 *             <ul class="sm list-unstyled" id="683a7b24-9000-4c8c-b885-4ca1bc195f62" role="menu">
 *               <li><a href="http://wet-boew.github.io/wet-boew/docs/bugs-en.html">Filing a bug or an issue</a></li>
 *             </ul>
 *           </li>
 *         </ul>
 *       </div>
 *     </div>
 *   </nav>
 */

( function() {
    "use strict";

    // For each top-level links in the megamenu
    $( ".wb-menu [role='menubar'] > li > a" ).each( function() {
        var $toplink = $( this ),
            $submenu = $toplink.siblings( ".sm" ).first(),
            guid; /* "Globally Unique IDentifier" */

        // If this top-level link has a sub-menu
        if ( $submenu.length > 0 ) {
            guid = wb.guid();                    /* generate a unique string */
            $toplink.attr( "href", "#" + guid ); /* use it as the top-link's href */
            $submenu.attr( "id", guid );         /* use it as the sub-menu's id */
        }
    } );
}() );
