/**
 * Responsive Breadcrumbs
 *
 * Implements section 6.2.4.2 of the Content and IA specification:
 *
 * > For small screen views, responsive breadcrumbs are used:  Where the
 * > breadcrumb trail contains four or more levels, intermediate links in the
 * > trail are replaced by an ellipsis to reduce the length of the trail
 * > displayed. The “Home” link is retained, as well as the last two links in
 * > the trail; remaining links must be replaced with ellipses.
 *
 * Refs:
 *  EPI-12989
 */

( function() {
    var MOBILE_SCREEN_SIZE = 767,
        MIN_RESPONSIVE_SIZE = 4,
        SECOND_INDEX = 1,
        ELLIPSES = "…",
        ELLIPSES_DATA_STRING = "ellipsesData",
        WB_BC_ID = "#wb-bc",
        init, restoreEllipsesInfo, setEllipses, setResponsiveInfo;

    init = function() {
        var list  = $( ".breadcrumb" ).children(),
            size  = list.size();

        if ( window.innerWidth <= MOBILE_SCREEN_SIZE && size >= MIN_RESPONSIVE_SIZE ) {
            list.each( function( index ) {
                setResponsiveInfo( this, index, size - 2 );
            } );
        } else {
            list.each( function( index ) {
                restoreEllipsesInfo( this, index, size );
                $( this ).show();
            } );
        }
    };

    // Restore original info that was replaced by the ellipses (...)
    restoreEllipsesInfo = function( obj, index ) {
        if ( index === SECOND_INDEX ) {
            if ( $( obj ).text() === ELLIPSES ) {
                $( obj ).html( ( $( WB_BC_ID ).data( ELLIPSES_DATA_STRING ) ).html() );
            }
        }
    };

    // Save the data that being replaced by the ellipses (...)
    setEllipses = function( obj ) {
        if ( $( obj ).text() !== ELLIPSES ) {
            $( WB_BC_ID ).data( ELLIPSES_DATA_STRING, $( obj ).clone( true ) );
            $( obj ).text( ELLIPSES );
        }
    };

    // Set ellipses and hide required Breadcrumb items
    setResponsiveInfo = function( obj, index, secondToLastIndex ) {
        if ( index === SECOND_INDEX ) {
            setEllipses( obj );
        } else {
            if ( index > SECOND_INDEX && ( index < secondToLastIndex ) ) {
                $( obj ).hide();
            }
        }
    };

    // on page "load"
    init();

    // on resize
    $( window ).on( "resize", init );
}() );
