/**
 * Prepend "?Open" to the query string of the WET's "Basic HTML" top link
 *
 * EPIC/Notes requires that the first argument in the query string be a known
 * command (e.g.: "Open"), otherwise throws a "Bad Request" error. The WET's
 * "Basic HTML" link is inserted with JS so we can't just hardcode this in the
 * template as HTML.
 *
 * Refs:
 *  https://github.com/wet-boew/wet-boew/blob/master/src/plugins/wb-disable/disable.js
 *  http://www.ibm.com/developerworks/lotus/library/ls-Domino_URL_cheat_sheet/
 */

( function() {
    "use strict";

    var intervalID,
        $tphp = $( "#wb-tphp" ),
        fixUrl = function() {
            var $link = $tphp.find( "[href*='wbdisable=']" ),
                href;

            if ( $link.length > 0 ) {

                // We found the link, stop checking for it
                clearInterval( intervalID );

                href = $link.attr( "href" );

                // If the link doesn't start with "?Open", add it
                if ( !( href.match( /^\?Open/ ) ) ) {
                    $link.attr( "href", href.replace( "?", "?Open=1&" ) );
                }
            }
        };

    // We need to check for the presence of this link in a timer since
    // we may get here before the WET has finished initializing and events
    // in "basic" mode are unreliable.
    intervalID = setInterval( fixUrl, 500 );
}() );
