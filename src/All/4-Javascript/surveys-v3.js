( function() {
    /*global ic_loop11: false, eic: false, pe: false*/
    "use strict";

    var cookie, bodyid,
        getURL = function() {
            var urls = ic_loop11.options.urls,
                nb_urls = urls.length;

            return urls[ Math.floor( Math.random() * nb_urls ) ];
        };

    cookie = eic.getCookie( ic_loop11.options.cookie );
    bodyid = $( "body" ).attr( "id" );

    if ( cookie === undefined && bodyid !== "_loop11__BODY1" ) {
        window.addEventListener( "load", function() {
            var dict = {
                    "title": ( pe.language === "en" ) ? "Usability Testing" : "Test d&#39;utilisabilit&eacute;",
                    "please": ( pe.language === "en" ) ? "Please help us improve the findability of this content by" : "Afin de nous aider &agrave; am&eacute;liorer la recherche d&#39;information &agrave; ce sujet, nous aimerions vous demander de",
                    "participate": ( pe.language === "en" ) ? "participating in our usability testing" : "participer &agrave; un test d&#39;utilisabilit&eacute;",
                    "asked": ( pe.language === "en" ) ? "You&#39;ll be asked to complete a few simple tasks. Don&#39;t worry, it won&#39;t take more than 10 minutes of your time." : "Vous n&#39;aurez qu&#39;&agrave; effectuer quelques op&eacute;rations bien simples. Cela vous prendra au plus 10 minutes.",
                    "take": ( pe.language === "en" ) ? "Take the test now" : "Participez au test maintenant",
                    "noThanks": ( pe.language === "en" ) ? "No thanks" : "Non merci",
                    "close": ( pe.language === "en" ) ? "Close" : "Fermer"
                },
                html =  "<div id='ic-loop11' style='border: 1px solid #ccc; background-color: #f5fffe; border-radius: 5px; box-shadow: 0px 5px 20px -5px; max-width: 440px; position: fixed; right: 20px; bottom: 20px; z-index: 1041;'>" +
                            "<header class='ic-loop11__header' style='border-radius: 5px 5px 0 0; background: url(http://ic.gc.ca/eic/site/icgc.nsf/vwimages/screens.png/$file/screens.png) 75% center no-repeat #298089; padding: 10px; border-bottom: 3px solid #898989;'>" +
                                "<h2 class='h5' style='background: transparent; position: static; color: #fff; display: inline;'>" + dict.title + "</h2>" +
                                "<button class='ic-loop11__close' style='cursor: pointer; color: #fff; border: 0; background: none; float: right;' title='" + dict.close + "'>x</button>" +
                            "</header>" +
                            "<div class='ic-loop11__body' style='padding: 15px;'>" +
                                "<p>" + dict.please + " <strong class='ic-strong' style='font-family: Arial; font-weight: bold;'>" + dict.participate + "</strong>. " + dict.asked + "</p>" +
                                "<p style='float: left;'><a class='ic-custom-link' style='color: #298089; font-family: Arial; font-weight: bold;' target='_blank' href='" + getURL() + "'>" + dict.take + "</a></p>" +
                                "<p style='float: right;'><a class='ic-loop11__close' href='#' style='color: #666;'>" + dict.noThanks + "</a></p>" +
                            "</div>" +
                        "</div>",
                $html = $( html );

            $html.find( ".ic-loop11__close" )
                .on( "click", function( e ) {
                    e.preventDefault();

                    $html.hide();
                } );

            $( "#wb-main-in" ).append( $html );

            eic.setCookie( ic_loop11.options.cookie, 1, 14 );
        } );
    }
}() );
