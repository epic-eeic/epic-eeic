/**
 * Interactive Toolbar
 *
 * Most of the styles/JS/markup come from WET's "Lightbox"[1]
 * and "Share widget"[2] features
 *
 * Here, we're just adding a "print" button at the start of the toolbar.
 * This is done via JS since when JavaScript is disabled, the feature can't
 * function, so shouldn't be there.
 *
 * SCSS:
 *  itb.scss
 *
 * Refs:
 *  EPI-10555
 *  http://icweb.ic.gc.ca/eic/site/er-rs.nsf/eng/00884.html#Toolbar
 *  [1] https://wet-boew.github.io/wet-boew/docs/ref/lightbox/lightbox-en.html
 *  [2] https://wet-boew.github.io/wet-boew/docs/ref/share/share-en.html
 */

( function() {
    "use strict";

    var $itb = $( ".eic-itb" ),
        lang = $( "html" ).attr( "lang" ),
        dic = {
            printTitle: ( lang === "en" ) ? "Print this page" : "Imprimer cette page",
            printText: ( lang === "en" ) ? "Print" : "Imprimer"
        };

    // Bail if there's no ITB on the page
    if ( $itb.length === 0 ) {
        return;
    }

    // Move the ITB just before the date modified (after the IC feedback, etc)
    $itb.insertBefore( "#wb-dtmd" );

    // Add print button
    $( "<li><button class='btn btn-link js-ic-print' title='" + dic.printTitle + "'><span class='glyphicon glyphicon-print'></span> " +  dic.printText + "</button></li>" )
        .on( "click", function( e ) {
            e.preventDefault();

            window.print();
        } )
        .prependTo( $itb );
}() );
