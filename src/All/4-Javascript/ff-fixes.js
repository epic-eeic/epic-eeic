/**
 * Stops video playback in Firefox when video is inside a modal window and the modal window is closed.
 * Feeds both _WET_4-0_utils.min.js and _WET_4-0_utils_canada.min.js
 *
 * Refs:
 *  EPI-12799
 */

( function() {
    if ( navigator.userAgent.toLowerCase().indexOf( "firefox" ) > -1 ) {
        document.querySelector( "body" ).addEventListener( "click", function( event ) {
            if ( event.target.className === "mfp-close" || event.target.className === "mfp-content" ) {
                $( "div.modal-body video" ).each( function() {
                    $( this ).get( 0 ).pause();
                } );
            }
        } );

        document.querySelector( "body" ).addEventListener( "keypress", function( event ) {
            if ( event.keyCode === 27 ) {
                $( "div.modal-body video" ).each( function() {
                    $( this ).get( 0 ).pause();
                } );
            }
        } );
    }
} )();
