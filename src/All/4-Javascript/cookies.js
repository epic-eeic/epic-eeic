/**
 * Utility functions for cookie management
 *
 * Required by:
 *  dismissible.js
 */

( function() {
    "use strict";

    var eic = window.eic || {};

    /**
     * Returns the value of the specified cookie, or undefined
     */
    eic.getCookie = function( name ) {
        var cookies = document.cookie.split( ";" ),
            len = cookies.length,
            i,
            value,
            cpair;

        for ( i = 0; i < len; i += 1 ) {
            cpair = cookies[ i ].split( "=" );

            if ( $.trim( cpair[ 0 ] ) === name ) {
                value = cpair[ 1 ];
                break;
            }
        }

        return value;
    };

    /**
     * Sets cookie
     */
    eic.setCookie = function( name, value, end ) {
        var expires = "",
            date;

        if ( end !== undefined ) {
            switch ( end.constructor ) {
                case Number:
                    date = new Date();
                    date.setTime( date.getTime() + ( end * 24 * 60 * 60 * 1000 ) );
                    break;
                case String:
                    date = new Date( end );
                    break;
            }

            if ( date instanceof Date ) {
                date.setHours( 0 );
                date.setMinutes( 0 );
                date.setSeconds( 0 );
                expires = "expires=" + date.toUTCString();
            }
        }

        document.cookie = name + "=" + value + ";" + expires + "; path=/";
    };

    window.eic = eic;
}() );
