/**
 * IC-specific Webtrends logic
 *
 * Refs:
 *  EPI-9814
 *  EPI-9242
 *  EPI-10035
 *  EPI-10354
 *  EPI-10141
 */

/*global endTime: false, startTime: false, WebTrends: false*/
( function( window, endTime, startTime, WebTrends ) {
    "use strict";

    var loadTime = ( endTime - startTime ) / 1000,
        $metrics = $( "#metrics" ),
        errs = [];

    window._tag = new WebTrends();
    window._tag.dcsGetId();
    window._tag.DCSext.loadTime = loadTime;

    // EPI-11304
    $( "span[id$='\\.errors'], span[id$='\\.error']" ).each( function() {
        errs.push( $( this ).attr( "id" ) );
    } );

    window._tag.WT.ic_appPgErrors = errs.join( "," );

    // EPI-9814 - Collect more info if we're on a corpcan page
    if ( window.location.pathname.match( /^\/app\/scr\/cc\/CorporationsCanada\// ) !== null ) {
        window._tag.dcsCustom = function() {

            // corpcan metrics
            window._tag.DCSext.metricView = $metrics.data( "metric-view" );
            window._tag.DCSext.metricAct = $metrics.data( "metric-act" );
            window._tag.DCSext.metricRequestType = $metrics.data( "metric-request-type" );
            window._tag.DCSext.metricConversationId = $metrics.data( "metric-conversation-id" );
            window._tag.DCSext.metricUserType = $metrics.data( "metric-user-type" );
            window._tag.DCSext.metricNameMethod = $metrics.data( "metric-name-method" );
            window._tag.DCSext.metricUrl = "/vpv/" + $metrics.data( "metric-request-type" ) + "/" + $metrics.data( "metric-view" ) + "/act-" + $metrics.data( "metric-act" );
        };
    } else {
        window._tag.dcsCustom = function() {

            /* EPI-10141 */
            window._tag.DCSext.epicFormPg = $( "input[name='$pg']" ).val();
        };
    }

    /* EPI-9242 */
    ( function fixRace() {

        /* hijack createimage so we can trigger an event once the image is loaded
           in reality we only need to wait until the server got the request */
        window._tag.dcsCreateImage = function( dcsSrc ) {
            var img = new Image();
            img.src = dcsSrc;
            img.style.display = "none";
            $( img )
                .imagesLoaded( function() {
                    $( document ).trigger( "ic.wt-image-loaded" );
                } )
                .appendTo( "body" );
        };

        // Tracked link is clicked
        // EPI-10354: Updated so we don't overwrite dcsMultiTrack
        $( "a[onclick^='dcsMultiTrack']" )
            .on( "click", function( e ) {
                e.preventDefault(); /* Don't follow link yet */

                var $this = $( this );

                // Once the WT image has been loaded, follow link
                $( document ).one( "ic.wt-image-loaded", function() {
                    if ( !$this.hasClass( "wt-js" ) ) {
                        window.location = $this.attr( "href" );
                    }
                } );
            } );
    }() );

    window._tag.dcsCollect();
}( window, endTime, startTime, WebTrends ) );
