( function() {
    "use strict";

    var FS,
        L11,
        dispatchPopup,
        eic = window.eic || {},
        ic_fluidsurveys = window.ic_fluidsurveys,
        ic_loop11 = window.ic_loop11;

    L11 = {
        canShow: function() {
            var cookie = eic.getCookie( ic_loop11.options.cookie ),
                bodyid = $( "body" ).attr( "id" );

            // Returns 'true' (e.g: we can show the popup) if the FS cookie doesn't exist
            return ( cookie === undefined && bodyid !== "_loop11__BODY1" );
        },

        getURL: function() {
            var urls = ic_loop11.options.urls,
                nb_urls = urls.length;

            return urls[ Math.floor( Math.random() * nb_urls ) ];
        },

        show: function() {
            var dict = {
                    "title": ( wb.lang === "en" ) ? "Usability Testing" : "Test d&#39;utilisabilit&eacute;",
                    "please": ( wb.lang === "en" ) ? "Please help us improve the findability of this content by" : "Afin de nous aider &agrave; am&eacute;liorer la recherche d&#39;information &agrave; ce sujet, nous aimerions vous demander de",
                    "participate": ( wb.lang === "en" ) ? "participating in our usability testing" : "participer &agrave; un test d&#39;utilisabilit&eacute;",
                    "asked": ( wb.lang === "en" ) ? "You&#39;ll be asked to complete a few simple tasks. Don&#39;t worry, it won&#39;t take more than 10 minutes of your time." : "Vous n&#39;aurez qu&#39;&agrave; effectuer quelques op&eacute;rations bien simples. Cela vous prendra au plus 10 minutes.",
                    "take": ( wb.lang === "en" ) ? "Take the test now" : "Participez au test maintenant",
                    "noThanks": ( wb.lang === "en" ) ? "No thanks" : "Non merci",
                    "close": ( wb.lang === "en" ) ? "Close" : "Fermer"
                },
                html =  "<div id='ic-loop11' style='border: 1px solid #ccc; background-color: #f5fffe; border-radius: 5px; box-shadow: 0px 5px 20px -5px; max-width: 440px; position: fixed; right: 20px; bottom: 20px; z-index: 1041;'>" +
                            "<header class='ic-loop11__header' style='border-radius: 5px 5px 0 0; background: url(http://ic.gc.ca/eic/site/icgc.nsf/vwimages/screens.png/$file/screens.png) 75% center no-repeat #298089; padding: 10px; border-bottom: 3px solid #898989;'>" +
                                "<h2 class='h5' style='color: #fff; display: inline;'>" + dict.title + "</h2>" +
                                "<button class='ic-loop11__close' style='color: #fff; border: 0; background: none; float: right;' title='" + dict.close + "'>x</button>" +
                            "</header>" +
                            "<div class='ic-loop11__body' style='padding: 15px;'>" +
                                "<p>" + dict.please + " <strong class='ic-strong' style='font-family: Arial; font-weight: bold;'>" + dict.participate + "</strong>. " + dict.asked + "</p>" +
                                "<p style='float: left;'><a class='ic-custom-link' style='color: #298089; font-family: Arial; font-weight: bold;' target='_blank' href='" + L11.getURL() + "'>" + dict.take + "</a></p>" +
                                "<p style='float: right;'><a class='ic-loop11__close' href='#' style='color: #666;'>" + dict.noThanks + "</a></p>" +
                            "</div>" +
                        "</div>",
                $html = $( html );

            $html.find( ".ic-loop11__close" ).on( "click", function( e ) {
                e.preventDefault();

                $html.hide();
            } );

            $( "main" ).append( $html );

            // Set cookie
            eic.setCookie( ic_loop11.options.cookie, 1, 14 );
        }
    };

    FS = {
        canShow: function() {
            var cookie = eic.getCookie( ic_fluidsurveys.options.cookie );

            // Returns 'true' (e.g: we can show the popup) if the FS cookie doesn't exist
            return ( cookie === undefined );
        },

        show: function() {
            $.getScript( "https://fluidsurveys-entv5.fs.cm/media/static/survey-prompts.js?" )
                .done( function() {
                    var SurveyPrompt = window.SurveyPrompt;

                    if ( SurveyPrompt ) {
                        window.FSPROMPT = new SurveyPrompt( ic_fluidsurveys.options ).setup();
                        jQuery.noConflict( true );
                    }
                } );
        }
    };

    dispatchPopup = function() {
        var fluidsurveys = false,
            loop11 = false;

        // Bail if we don't have any popups
        if ( ic_fluidsurveys === undefined && ic_loop11 === undefined ) {
            return;
        }

        // FS is a popup contender:
        //   it's present on the page and hasn't been completed
        if ( ic_fluidsurveys && FS.canShow() ) {
            fluidsurveys = true;
        }

        // Loop11 is a popup contender:
        //   it's present on the page and hasn't been completed
        if ( ic_loop11 && L11.canShow() ) {
            loop11 = true;
        }

        // If FS is a contender
        if ( fluidsurveys ) {
            if ( loop11 ) { // And loop11 is as well
                /* Each have a 50% chance of being shown */
                if ( Math.random() % 2 ) {
                    L11.show();
                } else {
                    FS.show();
                }
            } else { // Loop11 isn't a contender, but FS is
                FS.show(); // Show FS
            }
        } else if ( loop11 ) { // FS isn't a contender but Loop11 is
            L11.show(); // Show Loop11
        }
    };

    window.addEventListener( "load", dispatchPopup );
}() );
