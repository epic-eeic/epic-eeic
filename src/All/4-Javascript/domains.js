/**
 * Hiding content from view on different servers
 *
 * SCSS:
 *  domains.scss
 *
 * Refs:
 *  EPI-10734
 *  /eic/site/043.nsf/eng/00016.html#servers
 */

( function() {
    "use strict";

    var domain = window.location.host;

    if ( domain.match( /^(www\.)?icweb\.ic\.gc\.ca/ ) ) {
        $( ".icweb" ).show();
    } else if ( domain.match( /^(www\.)?stratpre1\.ic\.gc\.ca/ ) ) {
        $( ".stratpre1, .icweb, .icgcca" ).show();
    } else if ( domain.match( /^(www\.)?ic\.gc\.ca/ ) ) {
        $( ".icgcca" ).show();
    }
}() );
