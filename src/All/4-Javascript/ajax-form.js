/**
 * Submit an EPIC form via AJAX
 *
 * SCSS:
 *  _eic-ajax-form.scss
 *
 * Refs:
 *  EPI-11608
 */

( function() {
    "use strict";

    $( document ).on( "submit", ".eic-ajax-form", function( e ) {
        e.preventDefault();

        var $container = $( this ),
            $frm = $( e.target );

        $.post( $frm.attr( "action" ), $frm.serialize() )
            .done( function( data ) {
                var $response = $( data ).find( "#eic-form-wrapper" );

                $container
                    .attr( "tabindex", "-1" )
                    .html( $response.html() )
                    .focus();
            } )
            .fail( function() {
                $container.off( "submit" );
                $frm.trigger( "submit" );
            } );
    } );
}() );
