/**
 * Add a way to use the WET's form validation plugin in EPIC
 *
 * Through the "eic-frmvld" class on the <form> element (under the "Presentation" tab)
 *
 * Prompted by: EPI-11605
 */

( function() {
    "use strict";

    var $wrapper = $( ".eic-form-wrapper" ),
        $form = $wrapper.children( ".eic-frmvld" ).first();

    // There's nothing to do; bail.
    if ( $form.length === 0 ) {
        return;
    }

    // Add the proper WET class on the <form> wrapper
    $wrapper.addClass( "wb-frmvld" );

    // Call WET
    wb.add( ".wb-frmvld" );
}() );
