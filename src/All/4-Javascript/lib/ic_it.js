/**
 * JS file included by the CMS when the Interactive Toolbar is activated.
 */

/*global jQuery: false*/
(function($) {
    "use strict";
    
    var wetVersion = $('body').data('wetversion'),
        options = {
            dataType: "script",
            cache: true,
            url: '/eic/home.nsf/js/itb.js'
        };

    // If we're in a pre-v4.0 template, fetch itb.js
    if (wetVersion === undefined || wetVersion < 4) {
        $.ajax(options);
    }
    // Otherwise, we do nothing since the ITB code is included in the minified JS file.
}(jQuery));
