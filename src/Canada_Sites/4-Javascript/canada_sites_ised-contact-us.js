/**
 * THIS FILE IS NO LONGER USED - CMB controles the js files in icgc.nsf
 *This file sets hashes in the "Contact Us" link in the footer
 * of canada_sites template. This information is then used by
 * contactUs-v4.min.js (icgc.nsf) on page h_07026.html (icgc.nsf)
 */

/**
 * Utility functions to manipulate URL hashes
 *
 * For various reasons, we sometimes use the URL hash as a key/value
 * store (like a query string). These functions aim to make it easier
 * to extract/edit those keys/values.
 *
 * Required by:
 *  page-id.js
 */
( function() {
    "use strict";

    var eic = window.eic || {};

    eic.url = {

        /**
         * Add new parameters to a hash
         * @param   {jQuery|DOM|String}   target    elements to update
         * @param   {Object} newParams new parameters
         * @returns {jQuery} elements that have been updated
         */
        setHashParams: function( target, newParams ) {
            var $elm = ( target.jquery ) ? target : $( target );

            $elm.each( function() {
                var $this = $( this ),
                    elm = $this.get( 0 ),
                    params = eic.url.getHashParams( $this ),
                    hash = "",
                    href = $this.attr( "href" );

                // overwrite/add newParams data to link's current params
                $.each( newParams, function( key, val ) {
                    params[ key ] = val;
                } );

                // object to string
                $.each( params, function( key, val ) {
                    hash += "&" + key + "=" + val;
                } );

                // replace leading "&" with "#"
                hash = hash.replace( /^&/, "#" );

                // update href value
                if ( elm.hash === "" ) {
                    href += hash;
                } else {
                    href = href.replace( window.decodeURIComponent( elm.hash ), hash );
                }

                // update the element
                $this.attr( "href", href );
            } );

            // chaining
            return $elm;
        },

        /**
         * Get the keys/values in the hash of a targetted element
         *
         * Based on WET's wb.getUrlParts, but uses <url>.hash instead of
         * <url>.search
         *
         * @param   {jQuery|DOM|String}   target    element to get params from
         * @returns {Object} key/value map of the hash parameters
         */
        getHashParams: function( target ) {
            var elm = ( target.jquery ) ? target.get( 0 ) : target,
                keypairs = elm.hash.replace( /^#/, "" ).split( "&" ),
                len = keypairs.length,
                ret = {},
                keypair,
                i;

            for ( i = 0; i < len; i += 1 ) {
                if ( keypairs[ i ] ) {
                    keypair = keypairs[ i ].split( "=" );
                    ret[ keypair[ 0 ] ] = window.decodeURIComponent( keypair[ 1 ] );
                }
            }

            return ret;
        }
    };

    window.eic = eic;
} )();

/**
 * ICGCWEB-421: Contact us page ID
 *
 * Refs:
 *  ICGCWEB-421
 *  EPI-11122
 */

/**
 * This code was also modified to find and add the hash part
 * of Contact Us link on canada.ca template
 */
( function() {
    "use strict";

    /* From the DB got from URL, we decide which contact us items show up */
    var url_path = window.location.pathname;

    var pt = /[\-\da-z_]+\.nsf/;
    var db_name = pt.exec( url_path.replace( "/eic/site/", "" ) )[ 0 ];

    var page_name = url_path.split( "/" ).pop();

    var hashes;  // The hash values for Contact Us page URL

    if ( wb.lang === "en" ) {  // English
        hashes = {
            "013.nsf": { from: "Science", situation: "Choose" },
            "015.nsf": { from: "Internet", situation: "Professional" },
            "017.nsf": { from: "Other" },
            "020.nsf": { from: "Other", situation: "Other" },
            "021.nsf": { from: "Other" },
            "025.nsf": { from: "Internet", situation: "Amateur" },
            "026.nsf": { from: "Internet", situation: "Amateur" },
            "042.nsf": { from: "Industries" },
            "051.nsf": { from: "Science", situation: "Fund" },
            "054.nsf": { from: "Industries", situation: "General" },
            "058.nsf": { from: "Industries", situation: "General" },
            "061.nsf": { from: "Industries", situation: "SME" },
            "062.nsf": { from: "Innovation", situation: "Agenda" },
            "063.nsf": { from: "Science", situation: "Science" },
            "064.nsf": { from: "Media" },
            "067.nsf": { from: "Industries", situation: "General" },
            "068.nsf": { from: "Other" },
            "069.nsf": { from: "Science", situation: "Communications" },
            "071.nsf": { from: "Industries", situation: "General" },
            "075.nsf": { from: "Corporations", situation: "Nuans" },
            "080.nsf": { from: "Innovation" },
            "081.nsf": { from: "Other", situation: "All" },
            "086.nsf": { from: "Industries", situation: "General" },
            "693.nsf": { from: "Other" },
            "696.nsf": { from: "Financing" },
            "720.nsf": { from: "Internet" },
            "096.nsf": { from: "Innovation", situation: "Lab" },
            "107.nsf": { from: "Innovation", situation: "Women" },
            "114.nsf": { from: "Innovation", situation: "Women" },
            "ad-ad.nsf": { from: "Industries", situation: "General" },
            "ae-ve.nsf": { from: "Other" },
            "aimb-dgami.nsf": { from: "Industries", situation: "General" },
            "atip-aiprp.nsf": { from: "Other" },
            "auto-auto.nsf": { from: "Other" },
            "bsf-osb.nsf": { from: "Insolvency" },
            "cap-pac.nsf": { from: "Other" },
            "careers-carrieres.nsf": { from: "Other" },
            "cb-bc.nsf": { from: "Consumer", situation: "Misleading" },
            "ccc-rec.nsf": { from: "Industries", situation: "General" },
            "ccc_bt-rec_ec.nsf": { from: "Industries", situation: "General" },
            "cd-dgc.nsf": { from: "Corporations" },
            "ceb-bhst.nsf": { from: "Internet", situation: "certification" },
            "cfs-ope.nsf": { from: "Internet", situation: "General" },
            "chemicals-chimiques.nsf": { from: "Industries", situation: "General" },
            "cid-dic.nsf": { from: "Import", situation: "General" },
            "cilp-pdci.nsf": { from: "Other" },
            "cipointernet-internetopic.nsf": { from: "Intellectual" },
            "cis-sic.nsf": { from: "Industries", situation: "General" },
            "com-com.nsf": { from: "Other" },
            "csbfp-pfpec.nsf": { from: "Financing", situation: "Canada" },
            "csr-rse.nsf": { from: "Industries" },
            "dsib-dsib.nsf": { from: "Industries", situation: "General" },
            "dsib-logi.nsf": { from: "Industries", situation: "General" },
            "eas-aes.nsf": { from: "Industries", situation: "General" },
            "et-tdu.nsf": { from: "Internet", situation: "Emergency" },
            "hfc-hpc.nsf": { from: "Industries", situation: "General" },
            "ic-ic.nsf": { from: "Industries", situation: "General" },
            "ica-lic.nsf": { from: "Import", situation: "Act" },
            "iccat.nsf": { from: "Other", situation: "Catalogue" },
            "ich-epi.nsf": { from: "Other", situation: "General" },
            "icot-icto.nsf": { from: "Industries", situation: "General" },
            "ict-tic.nsf": { from: "Industries", situation: "General" },
            "ito-oti.nsf": { from: "Financing", situation: "Industrial" },
            "lsg-pdsv.nsf": { from: "Industries", situation: "General" },
            "mc-mc.nsf": { from: "Weights" },
            "mfg-fab.nsf": { from: "Industries", situation: "General" },
            "mra-arm.nsf": { from: "Internet", situation: "Mutual" },
            "oca-bc.nsf": { from: "Consumer" },
            "plastics-plastiques.nsf": { from: "Industries", situation: "General" },
            "pm-mp.nsf": { from: "Industries", situation: "General" },
            "pp-pp.nsf": { from: "Industries", situation: "General" },
            "retra-comde.nsf": { from: "Industries", situation: "General" },
            "rubber-caoutchouc.nsf": { from: "Industries", situation: "General" },
            "sd-dd.nsf": { from: "Other" },
            "sd-sd.nsf": { from: "Internet" },
            "sea-ees.nsf": { from: "Other" },
            "si-is.nsf": { from: "Industries", situation: "General" },
            "sim-cnmi.nsf": { from: "Industries", situation: "General" },
            "sms-sgs-preprod.nsf": { from: "Internet" },
            "sms-sgs-prod.nsf": { from: "Internet" },
            "smt-gst.nsf": { from: "Internet" },
            "tafl-ltaf.nsf": { from: "Internet", situation: "Technical" },
            "tapac-ccprt.nsf": { from: "Internet", situation: "Terminal" },
            "tdo-dcd.nsf": { from: "Import", situation: "General" },
            "textiles-textiles.nsf": { from: "Industries", situation: "General" }
        };
    } else { // French
        hashes = {
            "013.nsf": { from: "Sciences", situation: "Optez" },
            "015.nsf": { from: "Internet", situation: "professionnels" },
            "017.nsf": { from: "Autre" },
            "020.nsf": { from: "Autre", situation: "autres" },
            "021.nsf": { from: "Autre" },
            "025.nsf": { from: "Internet", situation: "radioamateur" },
            "026.nsf": { from: "Internet", situation: "radioamateur" },
            "042.nsf": { from: "Industries" },
            "051.nsf": { from: "Sciences", situation: "Fonds" },
            "054.nsf": { from: "Industries", situation: "Questions" },
            "058.nsf": { from: "Industries", situation: "Questions" },
            "061.nsf": { from: "Industries", situation: "Recherche" },
            "062.nsf": { from: "Innovation", situation: "Programme" },
            "063.nsf": { from: "Sciences", situation: "Science" },
            "064.nsf": { from: "Demandes" },
            "067.nsf": { from: "Industries", situation: "Questions" },
            "068.nsf": { from: "Autre" },
            "069.nsf": { from: "Sciences", situation: "communications" },
            "071.nsf": { from: "Industries", situation: "Questions" },
            "075.nsf": { from: "Entreprises", situation: "Nuans" },
            "080.nsf": { from: "Innovation" },
            "081.nsf": { from: "Autres", situation: "autres" },
            "086.nsf": { from: "Industries", situation: "Questions" },
            "693.nsf": { from: "Autre" },
            "696.nsf": { from: "Financement" },
            "720.nsf": { from: "Internet" },
            "096.nsf": { from: "Innovation", situation: "Labo" },
            "107.nsf": { from: "Innovation", situation: "femmes" },
            "114.nsf": { from: "Innovation", situation: "femmes" },
            "ad-ad.nsf": { from: "Industries", situation: "Questions" },
            "ae-ve.nsf": { from: "Autre" },
            "aimb-dgami.nsf": { from: "Industries", situation: "Questions" },
            "atip-aiprp.nsf": { from: "Autre" },
            "auto-auto.nsf": { from: "Autre" },
            "bsf-osb.nsf": { from: "Insolvabilité" },
            "cap-pac.nsf": { from: "Autre" },
            "careers-carrieres.nsf": { from: "Autre" },
            "cb-bc.nsf": { from: "consommation", situation: "trompeur" },
            "ccc-rec.nsf": { from: "Industries", situation: "Questions" },
            "ccc_bt-rec_ec.nsf": { from: "Industries", situation: "Questions" },
            "cd-dgc.nsf": { from: "Entreprises" },
            "ceb-bhst.nsf": { from: "Internet", situation: "certification" },
            "cfs-ope.nsf": { from: "Internet", situation: "Question" },
            "chemicals-chimiques.nsf": { from: "Industries", situation: "Questions" },
            "cid-dic.nsf": { from: "Importation", situation: "Question" },
            "cilp-pdci.nsf": { from: "Autre" },
            "cipointernet-internetopic.nsf": { from: "intellectuelle" },
            "cis-sic.nsf": { from: "Industries", situation: "Questions" },
            "com-com.nsf": { from: "Autre" },
            "csbfp-pfpec.nsf": { from: "Financement", situation: "financement" },
            "csr-rse.nsf": { from: "Industries" },
            "dsib-dsib.nsf": { from: "Industries", situation: "Questions" },
            "dsib-logi.nsf": { from: "Industries", situation: "Questions" },
            "eas-aes.nsf": { from: "Industries", situation: "Questions" },
            "et-tdu.nsf": { from: "Internet", situation: "urgence" }, //d"urgence
            "hfc-hpc.nsf": { from: "Industries", situation: "Questions" },
            "ic-ic.nsf": { from: "Industries", situation: "Questions" },
            "ica-lic.nsf": { from: "Importation", situation: "Loi" },
            "iccat.nsf": { from: "Autre", situation: "Répertoire" },
            "ich-epi.nsf": { from: "Autre", situation: "Questions" },  //générales
            "icot-icto.nsf": { from: "Industries", situation: "Questions" },
            "ict-tic.nsf": { from: "Industries", situation: "Questions" },
            "ito-oti.nsf": { from: "Financement", situation: "industrielles" },
            "lsg-pdsv.nsf": { from: "Industries", situation: "Questions" },
            "mc-mc.nsf": { from: "Poids" },
            "mfg-fab.nsf": { from: "Industries", situation: "Questions" },
            "mra-arm.nsf": { from: "Internet", situation: "mutuelle" },
            "oca-bc.nsf": { from: "consommation" },
            "plastics-plastiques.nsf": { from: "Industries", situation: "Questions" },
            "pm-mp.nsf": { from: "Industries", situation: "Questions" },
            "pp-pp.nsf": { from: "Industries", situation: "Questions" },
            "retra-comde.nsf": { from: "Industries", situation: "Questions" },
            "rubber-caoutchouc.nsf": { from: "Industries", situation: "Questions" },
            "sd-dd.nsf": { from: "Autre" },
            "sd-sd.nsf": { from: "Internet" },
            "sea-ees.nsf": { from: "Autre" },
            "si-is.nsf": { from: "Industries", situation: "Questions" },
            "sim-cnmi.nsf": { from: "Industries", situation: "Questions" },
            "sms-sgs-preprod.nsf": { from: "Internet" },
            "sms-sgs-prod.nsf": { from: "Internet" },
            "smt-gst.nsf": { from: "Internet" },
            "tafl-ltaf.nsf": { from: "Internet", situation: "Listes" },
            "tapac-ccprt.nsf": { from: "Internet", situation: "Terminal" },
            "tdo-dcd.nsf": { from: "Importation", situation: "Question" },
            "textiles-textiles.nsf": { from: "Industries", situation: "Questions" }
        };
    }

    // EPI-13807: Exception for 1 specific page in the 080.nsf db
    if ( db_name === "080.nsf" && page_name === "h_00027.html" ) {
        hashes[ db_name ].situation = ( wb.lang === "en" ) ? "Funding" : "Financement";
    }

    /* Below is the modified code to update the Contact us URL by adding hash */
    var $footer = $( "#wb-info" ),
        $footerSections = $footer.find( "nav[role='navigation']" ),
        contactUs = ( wb.lang === "en" ) ? "contact us" : "contactez-nous",
        pageid,
        $elms = $( "" ),
        eic = window.eic || {};

    // ICGCWEB-421: If we're on the contact us page, take pageid from url if present
    if ( document.getElementById( "contactUs" ) !== null ) {
        pageid = eic.url.getHashParams( window.location.href ).pageid;
    } else { /* Otherwise, take it from .page-id */
        pageid = $footer.find( ".page-id" ).text().replace( /Page\s?: /, "" );
    }

    // Couldn't find pageid, bail.
    if ( !pageid ) {
        return;
    }

    $footerSections.each( function() {
        var $section = $( this ),
            $heading = $section.find( "ul li" );

        $heading.each( function() {

            var $li = $( this );
            var $contact_us_item = $li.find( "a" );
            if ( $contact_us_item.text().toLowerCase() === contactUs ) {

                if ( hashes[ db_name ] ) {

                    if ( hashes[ db_name ].from ) {
                        eic.url.setHashParams( $contact_us_item, { from: hashes[ db_name ].from } );
                    }

                    if ( hashes[ db_name ].situation ) {
                        eic.url.setHashParams( $contact_us_item, { situation: hashes[ db_name ].situation } );
                    }

                    if ( hashes[ db_name ].enquiry ) {
                        eic.url.setHashParams( $contact_us_item, { enquiry: hashes[ db_name ].enquiry } );
                    }

                    $elms = $contact_us_item;

                    return false;
                }
            }
        } );
    } );

    // Add the pageid parameter to the hash params
    eic.url.setHashParams( $elms, { "pageid": pageid } );

} )();
