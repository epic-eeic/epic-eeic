/**
 * Move the EPIC-generated 'archived' HTML to the proper location in the
 * Canada template
 *
 * Refs:
 *  EPI-12891
 */

( function() {
    var $archivedWrapper = $( "#eic-archived" ),
        $archivedContentContainer = $archivedWrapper.children( ".container" );

    // Move the archived link, if present, in the archived container
    $( "#eic-archived-link p" ).appendTo( $archivedContentContainer );

    // Move the archived container higher
    $archivedWrapper.insertBefore( "#wb-tphp" );

    $archivedContentContainer
        .clone( true )           // Duplicate the content container
        .wrap(                   // Add the proper container around it
          "<section class='gc-archv wb-inview modal-content show-none' data-inview='eic-archived'></section>"
        )
        .parent()                // Point to the <section> elm we just created
        .insertBefore( "#eic-archived" ); // Insert it in the proper location
}() );
