/**
 * Utility functions to manipulate URL hashes test
 *
 * For various reasons, we sometimes use the URL hash as a key/value
 * store (like a query string). These functions aim to make it easier
 * to extract/edit those keys/values.
 *
 * Required by:
 *  page-id.js
 */

( function() {
    "use strict";

    var eic = window.eic || {};

    eic.url = {

        /**
         * Add new parameters to a hash
         * @param   {jQuery|DOM|String}   target    elements to update
         * @param   {Object} newParams new parameters
         * @returns {jQuery} elements that have been updated
         */
        setHashParams: function( target, newParams ) {
            var $elm = ( target.jquery ) ? target : $( target );

            $elm.each( function() {
                var $this = $( this ),
                    elm = $this.get( 0 ),
                    params = eic.url.getHashParams( $this ),
                    hash = "",
                    href = $this.attr( "href" );

                // overwrite/add newParams data to link's current params
                $.each( newParams, function( key, val ) {
                    params[ key ] = val;
                } );

                // object to string
                $.each( params, function( key, val ) {
                    hash += "&" + key + "=" + val;
                } );

                // replace leading "&" with "#"
                hash = hash.replace( /^&/, "#" );

                // update href value
                if ( elm.hash === "" ) {
                    href += hash;
                } else {
                    href = href.replace( elm.hash, hash );
                }

                // update the element
                $this.attr( "href", href );
            } );

            // chaining

            return $elm;
        },

        /**
         * Get the keys/values in the hash of a targetted element
         *
         * Based on WET's wb.getUrlParts, but uses <url>.hash instead of <url>.search
         *
         * @param   {jQuery|DOM|String}   target    element to get params from
         * @returns {Object} key/value map of the hash parameters
         */
        getHashParams: function( target ) {
            var elm = ( target.jquery ) ? target.get( 0 ) : target,
                keypairs = elm.hash.replace( /^#/, "" ).split( "&" ),
                len = keypairs.length,
                ret = {},
                keypair,
                i;

            for ( i = 0; i < len; i += 1 ) {
                if ( keypairs[ i ] ) {
                    keypair = keypairs[ i ].split( "=" );
                    ret[ keypair[ 0 ] ] = keypair[ 1 ];
                }
            }

            return ret;
        }
    };

    window.eic = eic;
} )();


/**
 * ICGCWEB-421: Contact us page ID
 *
 * Refs:
 *  ICGCWEB-421
 *  EPI-11122
 */


/**
 * This code was also modiflesd
 * to find and add the hash part
 * of Contact Us link
 * on canada.ca template
 */

( function() {
    "use strict";

    /* From the DB got from URL, we decide which contact us items show up */
    var url_path = window.location.pathname;

    var pt = /[\-\da-z_]+\.nsf/;
    var db_name = pt.exec( url_path.replace( "/eic/site/", "" ) )[ 0 ];

    var hashes;  // The hash values for Contact Us page URL

    if ( wb.lang === "en" ) { // ENG
        hashes = {
            "013.nsf": { from: "Science", situation: "Choose" },
            "062.nsf": { from: "Innovation", situation: "Agenda" },
            "075.nsf": { from: "Corporations", situation: "Nuans" }
        };
    } else { // FRA

        hashes = {
            "013.nsf": { from: "Sciences", situation: "Optez" },
            "062.nsf": { from: "Innovation", situation: "Programme" },
            "075.nsf": { from: "Entreprises", situation: "Nuans" }
        };
    }

    /* Below is the modified code to update the Contact us URL by adding hash */

    var $footer = $( "#wb-info" ),
        $footerSections = $footer.find( ".brand .container" ),
        contactUs = ( wb.lang === "en" ) ? "contact information" : "coordonnées",
        pageid,
        $elms = $( "" ),
        eic = window.eic || {};

    // ICGCWEB-421: If we're on the contact us page, take pageid from url if present
    if ( document.getElementById( "contactUs" ) !== null ) {
        pageid = eic.url.getHashParams( window.location.href ).pageid;
    } else { /* Otherwise, take it from .page-id */
        pageid = $footer.find( ".page-id" ).text().replace( /Page\s?: /, "" );
    }

    // Couldn't find pageid, bail.
    if ( !pageid ) {
        return;
    }

    $footerSections.each( function() {
        var $section = $( this ),
            $heading = $section.find( "ul li" );

        $heading.each( function() {
            var $li = $( this );
            var $contact_us_item = $li.find( "a" );
            if ( $contact_us_item.text().toLowerCase() === contactUs ) {
                if ( hashes[ db_name ] ) {
                    if ( hashes[ db_name ].from ) {
                        eic.url.setHashParams( $contact_us_item, { from: hashes[ db_name ].from } );
                    }

                    if ( hashes[ db_name ].situation ) {
                        eic.url.setHashParams( $contact_us_item, { situation: hashes[ db_name ].situation } );
                    }

                    if ( hashes[ db_name ].enquiry ) {
                        eic.url.setHashParams( $contact_us_item, { enquiry: hashes[ db_name ].enquiry } );
                    }

                    $elms = $contact_us_item;

                    return false;
                }
            }
        } );
    } );

    // Add the pageid parameter to the hash params
    eic.url.setHashParams( $elms, { "pageid": pageid } );

} )();
