Documentation on how the local web pages are built
========================

This folder and subfolders are used by the "string-replace" and "assemble" grunt task (see Gruntfile.js)  
Documentation on assemble can be found here: http://assemble.io/docs/

string-replace
------------------------

EPIC uses its own syntax for variables (ex: !BANNER!).  
The first thing we want to do is convert those to a known format so they're easier to work with afterwards.  
This is what the "string-replace" Grunt task does:

1. Converts EPIC generated content and EPIC variables into handlebars partials format ( http://handlebarsjs.com/#partials ).
2. Converts DB/page variables (DEFVAR, BANNER, etc.) into handlebar variables.

For example, it'll replace "!BREADCRUMB!" with "{{>epic.breadcrumb-eng}}" in the following snippet from "src/IC_Sub_Sites/1-Master Templates/2-header-1col-eng.html"

```html
<ol class="breadcrumb">
    !BREADCRUMB!
    <!-- EPIC_DYNAMIC_TAG KEY="EPIC_BREADCRUMB" --><!-- /EPIC_DYNAMIC_TAG -->
</ol>
```

The resulting files are in handlebars format (.hbs) and are saved in ```build/**/layout/**``` (ex: "build/IC_Sub_Sites/layout")  
More information about layouts can be found here: http://assemble.io/docs/Layouts.html

assemble
------------------------

Then, we tell Grunt to run the "assemble" task.  
This task takes all the different template pieces and glues them together in complete HTML files.

If we go through the process of building the "IC_Sub_Sites" template, it looks like this:

```js
ic_subsites: {
    options: {
        data: "build/IC_Sub_Sites/data.yml",
        layoutdir: "build/IC_Sub_Sites/layout",
        partials: "build/IC_Sub_Sites/includes/**/*.html"
    },
    files: [
        {
            expand: true,
            src: [ "src/IC_Sub_Sites/pages/**/*.html", "src/All/pages/**/*.html", "build/localhost/page-list-eng.html" ],
            dest: "demos/eic/templates/IC_Sub_Sites/",
            rename: function( destBase, destPath ) {
                return destBase + destPath.replace( /.+(\/[a-z]+\/.+\.html)/i, "$1" );
            }
        }
    ]
},
```

Grunt takes all the files in the "src/IC_Sub_Sites/pages/", "src/All/pages/" folders and subfolders ending with ".html"

Those files are in handlebars format ( http://handlebarsjs.com/ ) starting with an optional YAML Front Matter ( YFM: http://assemble.io/docs/YAML-front-matter.html ) section.

For each of those pages, grunt will be using the layouts (handlebar templates generated in the section above) found in "build/IC_Sub_Sites/layout" as a starting point.

The variables in those layouts ( {{{ head.DEFVAR1 }}}, etc. ) will be replaced with the data in "build/IC_Sub_Sites/data.yml" as the default values.  
Those variable can be overwritten in the YFM on a per-page basis.  
For example, to overwrite the default subsite banner ("The banner value" as defined in "build/IC_Sub_Sites/data.yml"), you would add the following at the top of your page:

```yaml
---
title: The title of the page

config:
    BANNER: <a href="#">Linked banner</a>
---
```

Finally, the partials in those layouts ( {{>epic.breadcrumb-eng}} ) will be replaced with the data in ```build/IC_Sub_Sites/includes/**/.html```

For example, the breadcrumbs partial will be replaced with the content in the "epic.breadcrumb-eng.html" file:

```html
<li>
    <a href="http://wet-boew.github.io/v4.0-ci/index-en.html">Home</a>
</li>
<li>
    <a href="http://wet-boew.github.io/v4.0-ci/demos/index-en.html">Working examples</a>
</li>

<li>
    <a href="./index-en.html">GCWU theme</a>
</li>

<li>Content page - Secondary menu</li>
```

The complete HTML pages are saved in "demos/eic/templates/IC_Sub_Sites/", which can be browsed via the local HTTP server ( http://127.0.0.1:8080 ) when the "grunt server" task is running.
